﻿
namespace XELibrary
{
    public interface ISoundManager
    {
        bool RepeatPlayList { get; set; }

        bool IsSongPlaying(string soundName);
        void Play(string soundName);
        void Play(string soundName, float v);
        void PauseSong();
        void ResumeSong();
        void StopAll();
        void StopSong();
        void StartPlayList(string[] playList, int startIndex = 0);
        void StartPlayList(int startIndex);
        void StopPlayList();
    }
}
