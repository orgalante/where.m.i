using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XELibrary
{
    /// <summary>
    /// Camera abstract class
    /// </summary>
    public class Camera : Microsoft.Xna.Framework.GameComponent
    {
        public Matrix Projection { get; private set; }
        public Matrix View { get; private set; }
        public Vector3 CameraPosition;
        public Vector3 CameraTarget { get; set; } = Vector3.Zero;
        public Vector3 CameraUp { get; } = Vector3.Up;
        public float NearZ { get; } = 1.0f;
        public float FarZ { get; } = 1000.0f;
        Vector3 cameraDirection;
        //defines speed of camera movement
        float speed = 0.15F;
        float speedSides = 0.05F;

        public Camera(Game game, Vector3 pos, Vector3 target)
            : base(game)
        {
            CameraPosition = pos;
            cameraDirection = target - pos;
            cameraDirection.Normalize();
        }

        public override void Initialize()
        {

            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)Game.Window.ClientBounds.Width / (float)Game.Window.ClientBounds.Height, NearZ, FarZ);

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                CameraPosition += cameraDirection * speed;
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                CameraPosition -= cameraDirection * speed;

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                cameraDirection += Vector3.Cross(CameraUp, cameraDirection) * speedSides;
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                cameraDirection -= Vector3.Cross(CameraUp, cameraDirection) * speedSides;

            View = Matrix.CreateLookAt(CameraPosition, CameraPosition + cameraDirection, CameraUp);

            base.Update(gameTime);
        }
    }
}
