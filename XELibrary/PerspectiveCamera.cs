using Microsoft.Xna.Framework;

namespace XELibrary
{
    /// <summary>
    /// Simple orthographic camera. You can use this camera to create simple 2D scenes.
    /// </summary>
    public class PerspectiveCamera : Camera
    {
        public PerspectiveCamera(Game game)
            : base(game)
        {
        }

        protected override Matrix InitializeProjection()
        {
            //Projection
            float aspectRatio =
                (float)Game.GraphicsDevice.Viewport.Width /
                (float)Game.GraphicsDevice.Viewport.Height;

            return Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, aspectRatio, NearZ, FarZ);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


    }
}
