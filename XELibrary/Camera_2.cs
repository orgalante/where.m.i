using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;

namespace XELibrary
{
    /// Camera_2 is a game component that controls the view position and direction of the player in the games first level.
    /// the camera class can do the following:
    /// -get keyboard input: right, left,up,down arrows.
    /// -calculate the next step or the next view direction.
    /// -create a new view to draw based on its calculations. 

    public class Camera_2 : Microsoft.Xna.Framework.GameComponent
    {
        public Matrix Projection { get; private set; }
        public Matrix View { get; set; }
        public Vector3 CameraPosition { get; set; } = Vector3.UnitZ;
        public Vector3 CameraTarget { get; set; } = Vector3.Zero;
        public Vector3 CameraUp { get;} = Vector3.Up;
        public float NearZ { get; } = 1.0f;
        public float FarZ { get; } = 1000.0f;
        public float stepHeight = 0;

        public Vector3 cameraDirection;//{ get; set; }
        public Vector3 calculatedPosition { get; set; } = Vector3.UnitZ;
        public bool canWalk { get; set; }

        //defines speed of camera movement
        float speed = 0.8F;
        float speedSides = 0.05F;

        public Camera_2(Game game, Vector3 pos, Vector3 target)
            : base(game)
        {

            CameraPosition = pos;
            cameraDirection = target - pos;
            cameraDirection.Normalize();
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)Game.Window.ClientBounds.Width / (float)Game.Window.ClientBounds.Height, NearZ, FarZ);
        }

        public void updateProj()
        {
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)Game.Window.ClientBounds.Width / (float)Game.Window.ClientBounds.Height, NearZ, FarZ);
        }

        public override void Initialize()
        {
            //Projection
           
            base.Initialize();
        }
   
        public override void Update(GameTime gameTime)
        {
            // Move forward and backward
            float y = (float)Math.Sin(stepHeight*50 * (Math.PI / 180))*0.02f;
            calculatedPosition = CameraPosition+new Vector3(0,y,0);

            if (canWalk)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                {
                    calculatedPosition += cameraDirection * speed;

                }
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    calculatedPosition -= cameraDirection * speed;
                }
            }

            
          

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                cameraDirection += Vector3.Cross(CameraUp, cameraDirection) * speedSides;
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                cameraDirection -= Vector3.Cross(CameraUp, cameraDirection) * speedSides;

            cameraDirection.Normalize();
            
           // View = Matrix.CreateLookAt(CameraPosition, CameraTarget, CameraUp);

            base.Update(gameTime);
        }

        public void CreateLookAt()
        {
            CameraTarget = CameraPosition + cameraDirection;
            View = Matrix.CreateLookAt(CameraPosition, CameraTarget, CameraUp);
        }

        public  string CameraToString()
        {
            string str = "camera position " + (int)CameraPosition.X + "," + (int)CameraPosition.Y + "," + (int)CameraPosition.Z + "\n";
            str += "direction: " + (int)cameraDirection.X + "," + (int)cameraDirection.Y + "," + (int)cameraDirection.Z + "\n";
            return str;
        }

    }
}
