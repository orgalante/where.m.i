﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    /// Chapter2State is the state that controls every aspect of the second level in the game.
    /// It has an open-world of forest of bones and in its center a cage made of bones.
    /// in the cage theres a monster. the stage has: monster object, ground model, cage model, grid object
    /// and a camera object to draw the level from first person view.
    /// the state controls actions between his objects, the level game play and moving to other states if has to or finished successfully.
    /// in order to pass this level the player has to open the cage and then to attack and monster.
    /// to open the cage - player need to break the bones by pressing R & L on keyboard.
    /// to attack the monster- player need to move bone and then hit it by pressing A for 4 times.
    /// to move a bone - player need to stand next to a bone and then press M.
    /// if the monster have no bone to track, it attacks the player- if touches it-player is dead.
    public sealed class Chapter2State : BaseGameState, IChapter2State
    {
        // chapter2 parameters
        public Camera_2 camera;
        public SpriteBatch spriteBatch;
        private SpriteFont announceFont;
        Player_chp1 player;
        Monster_chp2 monster;
        Matrix world;
        BoundingSphere cameraBS;
        Vector3 cageDoor = new Vector3(0, 15, 35);
        public float playerFromMonster = 200;
        ISoundManager soundManager;
        private bool playSound;
        string[][] mapStr;
        public double lastBoneSound = 0;
        Grid grid { get; set; }
        public List<Game1.ModelClass> staticModelsListS2 = new List<Game1.ModelClass>(10);
        public List<Game1.ModelClass> bonesModelsList = new List<Game1.ModelClass>(10);
        public Game1.ModelClass ground { get; private set; }
        public Game1.ModelClass cage { get; private set; }
        public Game1.ModelClass throwBone { get; private set; }
        public double secFromStart { get; private set; }
        private bool showCageString;
        private bool isRight = true;
        int bonesLeft = 6;
        private bool showMstr;
        bool playerAttack = false;
        private bool showAstr;
        private bool IsMonsterAfterBone = false;
        private bool IsPlayerDead = false;
        private int currMovingBoneIndx = -1;

        // CHAPTER2 STATE
        public Chapter2State(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IChapter2State), this); //add this state to game service.
            // Sound
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
            // World 
            world = Matrix.Identity;
            //  grid build
            mapStr = loadCsvFile("chapter2map.csv");
            grid = new Grid(Game, mapStr.Length, mapStr[0].Length);
            OurGame.Components.Add(grid);
            // player build
            player = new Player_chp1(game);
            OurGame.Components.Add(player);
            // monster build
            monster = new Monster_chp2(game);
            OurGame.Components.Add(monster);
            // states independent models
            ground = new Game1.ModelClass();
            cage = new Game1.ModelClass();
            throwBone = new Game1.ModelClass();
            //function to initialize values
            initializeValues();
        }

        // this method reset the state parameters to its initial values.
        public override void Initialize()
        {
            initializeValues();
            base.Initialize();
        }
        // this method reset the state parameters to its initial values.
        void initializeValues()
        {
            // Load sounds
            string musicPath = @"Music\Songs";
            string fxPath = @"Music\Sounds";
            OurGame.soundManager.LoadContent(musicPath, fxPath);
            // Camera
            if (camera != null)
                camera.Dispose();
            Vector3 camPosition = grid.r_cToPosition(4, 4) + new Vector3(0, 15, 0); // position of camera/player in world
            Vector3 camLookTo = grid.r_cToPosition(0, 0) + new Vector3(0, 15, 0); // position of target to look at in world
            camera = new Camera_2(OurGame, camPosition, camLookTo); // camera first initialize
            // player build
            player.World = world;
            player.Camera = camera;
            player.Position = camera.CameraPosition;
            // monster build
            monster.World = world;
            monster.Position = Vector3.Zero;
            monster.Camera = camera;
            monster.playerPosition = player.Position;
            monster.grid = grid;
        }

        // this method load contents such as models, fonts and sounds and conects them to this state parameters
        protected override void LoadContent()
        {
            // this playlist plays backsound
            string[] playList = { "Gore Ambience Sketch" };
            soundManager.StartPlayList(playList, 0);
            soundManager.RepeatPlayList = true; // play back sound.
            // add sprite batch
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // ground build
            ground.model = Game.Content.Load<Model>(@"Models\Earth");
            ground.effect = ground.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            ground.position = Vector3.Zero; // 
            ground.Transform = Matrix.CreateTranslation(Vector3.Zero) * Matrix.CreateScale(new Vector3(200, 0.1f, 200)) * world;
            ground.BB = BuildBoundingBox(ground.model, ground.Transform);
            //cage build
            cage.model = Game.Content.Load<Model>(@"Models\caveBone6");
            cage.effect = ground.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            cage.position = Vector3.Zero; // 
            cage.Transform = Matrix.CreateTranslation(Vector3.Zero) * world;
            cage.BB = BuildBoundingBox(cage.model, cage.Transform);
            // throwBone build - this bone is thrown to monster from player
            throwBone.model = Game.Content.Load<Model>(@"Models\bone_1");
            throwBone.effect = ground.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            throwBone.position = Vector3.Zero; // 
            throwBone.Transform = Matrix.CreateTranslation(Vector3.Zero) * world;
            // font is load for printing on screen for player or for debug.
            announceFont = Game.Content.Load<SpriteFont>(@"Fonts\StartMenuFont");
            camera.CreateLookAt(); // creates first look
            csvStrMapToModels(); // reads the state map and translate it to grid
            csvStrToModels(loadCsvFile("chapter2.csv")); // reads some static models settings
            base.LoadContent();
        }

        // read csv file function
        public string[][] loadCsvFile(string file_name)
        {
            // Read each line of the file into a string array. 
            var csvFilePath = Directory.GetCurrentDirectory() + "\\Content\\Files\\" + file_name;
            return File.ReadLines(csvFilePath).Where(line => line != "").Select(x => x.Split(',')).ToArray();
        }
        // translate csv-string to actual models, positions, scales
        public void csvStrToModels(string[][] str)
        {
            for (int r = 1; r < str.Length; r++) // we start from row 1 since 0 is headline
            { //for each row
                float[] pArr = new float[6];
                for (int c = 0; c < 6; c++) //for each column
                    pArr[c] = (float)Convert.ToDouble(str[r][c]);
                if (str[r][6] != "Earth")
                    addModel(str[r][6], new Vector3(pArr[0], pArr[1], pArr[2]), new Vector3(pArr[3], pArr[4], pArr[5]));
            }
        }
        // translate the map-csv-string-array to actual grid with models locations 
        public void csvStrMapToModels()
        {
            grid.Camera = camera;
            grid.World = world;
            for (int r = 0; r < grid.rows; r++) //for each row
                for (int c = 0; c < grid.columns; c++)
                { //for each column         
                    if (mapStr[r][c] != "0")
                        addModel(mapStr[r][c], grid.r_cToPosition(r, c), new Vector3(1, 1, 1));
                }
        }

        // This function chacks if the player/camera is moving inside the game boundries
        bool CameraBoundries()
        {
            if (camera.calculatedPosition.X < (-1 * grid.limitX) + 1)
                return false;
            else if (camera.calculatedPosition.X > grid.limitX - 1)
                return false;
            if (camera.calculatedPosition.Z < (-1 * grid.limitZ) + 1)
                return false;
            else if (camera.calculatedPosition.Z > grid.limitZ - 1)
                return false;
            else
            { // checks if player/camera too close to the cage model.
                if (cameraBS.Intersects(cage.BB))
                    return false;
                else
                    return true;
            }
        }
        // this function get model and add it to the relevant list of models
        public void addModel(string modelName, Vector3 pos, Vector3 scale)
        {
            Game1.ModelClass newM = new Game1.ModelClass();
            newM.model = Game.Content.Load<Model>(@"Models\" + modelName);
            newM.effect = newM.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            newM.position = pos;
            newM.Transform = Matrix.CreateTranslation(pos) * Matrix.CreateScale(scale) * world;
            newM.BB = BuildBoundingBox(newM.model, newM.Transform);
            // if one of the bones around world need to go into bonesModelsList
            if (modelName == "bone_1" || modelName == "bone_2")
            {
                bonesModelsList.Add(newM);
                grid.addBoneModel(newM);
            }
            else // else go into staticModelsListS2
            {
                staticModelsListS2.Add(newM);
                grid.addModel(newM);
            }
        }
        // switch the cage model according to bones left to crack from cage.
        public void cageModel()
        {
            soundManager.Play("BoneCrush1", 1f); // plays crack sound  
            bonesLeft -= 1;
            cage.model = Game.Content.Load<Model>(@"Models\caveBone" + bonesLeft);
            cage.effect = ground.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            cage.position = Vector3.Zero; // 
            cage.Transform = Matrix.CreateTranslation(Vector3.Zero) * world;
            cage.BB = BuildBoundingBox(ground.model, ground.Transform);
            if (bonesLeft == 0)
                monster.canMove = true; // monster can move out of the cage
        }
        // sets to monster a new destination 
        public void monsterSetDest(Vector3 dest)
        {
            monster.canMove = true;
            monster.IsNewPath = true;
            monster.destination = dest;
        }

        // UPDATE every frame of this state
        public override void Update(GameTime gameTime)
        {
            playerFromMonster = Vector3.Distance(monster.Position, camera.CameraPosition);
            showMstr = false; // to initialize the check
            IsPlayerDead = ((playerFromMonster < 30) && monster.attacked);

            //if finished the level successfully ->end level
            if (monster.IsDead)
            {
                monster.paused = true; //so the monster will stop moving
                soundManager.StopPlayList();
                StateManager.ChangeState(OurGame.FinishStageMenuState.Value);  // moving the user to the Finish Menu
            }
            // if monster model attack and touches the player-> game ended
            else if (IsPlayerDead)
            {
                soundManager.StopPlayList();
                StateManager.PopState();
                StateManager.ChangeState(OurGame.StartMenuState.Value);
            }
            // if player press p -> game paused.
            else if (Input.KeyboardHandler.WasKeyPressed(Keys.P))
            {
                camera.canWalk = false;
                // StateChanged takes care of pausing state and monster.
                StateManager.PushState(OurGame.DistruptionMenuState.Value); // Push the user to the distruption menu
            }
            // if play mode
            else
            {
                bool boneActivated = false;
                if (monster.canMove)
                {
                    // this loop check every dynamic model if player can operate it
                    foreach (Game1.ModelClass m in grid.bonesModelsList)
                    {
                        float playerFromBone = Vector3.Distance(m.position, camera.CameraPosition);
                        // 
                        if (playerFromBone < 50 && !boneActivated)
                        {
                            showMstr = true;
                            // when we press m we make the bone the new destination for monster
                            if (Input.KeyboardHandler.WasKeyPressed(Keys.M) && !m.action)
                            {
                                // makes sure only one bone can move during the game
                                if (currMovingBoneIndx != -1)
                                    grid.bonesModelsList[currMovingBoneIndx].action = false;
                                currMovingBoneIndx = grid.bonesModelsList.IndexOf(m);
                                m.action = true;
                                monsterSetDest(m.position);
                                IsMonsterAfterBone = true;
                                monster.attacked = false;
                                boneActivated = true;
                            }
                        }
                        if (m.action) // if bone is moved
                        {
                            // play bone moving sound
                            secFromStart = gameTime.TotalGameTime.TotalSeconds;
                            float volume = (20 / playerFromBone);
                            if (secFromStart - lastBoneSound > 2 && playSound && volume <= 1)
                            {
                                lastBoneSound = secFromStart;
                                soundManager.Play("Squish1", volume); // plays bone sound
                            }
                            // move the bone model 360 degrees so when come to 360 change back to 0
                            if (m.angle > 360)
                                m.angle = 0;
                            m.angle++;
                            m.Transform = Matrix.CreateRotationY((float)(Math.PI / 180) * m.angle) * Matrix.CreateTranslation(m.position) * world;
                            // if monster near the bone it stops moving
                            if (Vector3.Distance(m.position, monster.Position) < 15)
                                m.action = false;
                        }
                    }

                    // if monster is out , alive and not attacking player, we show option for player to attack the monster
                    if (!playerAttack && !monster.gotHit && !monster.attacked)
                    {
                        showAstr = ((playerFromMonster < 100) && (IsMonsterAfterBone));
                        if (Input.KeyboardHandler.WasKeyPressed(Keys.A))
                        {
                            throwBone.position = camera.CameraPosition;
                            playerAttack = true;
                        }
                    }
                    else if (playerAttack)
                    {
                        float elapsed = 0.5f;
                        Vector3 direction = Vector3.Normalize((monster.Position + new Vector3(0, 7, 0)) - throwBone.position);
                        throwBone.position += direction * elapsed;
                        throwBone.Transform = Matrix.CreateTranslation(throwBone.position) * world;
                        if (Vector3.Distance(monster.Position, throwBone.position) < 10)
                        {
                            monster.gotHit = true;
                            playerAttack = false;
                            // play hit sound
                            soundManager.Play("Splash1", 1f);
                        }
                    }

                }
                else // getting monster out of the cage
                {
                    //this loop is to open the entrance to the cage
                    if (Vector3.Distance(cageDoor, camera.CameraPosition) < 10 && bonesLeft > 0)
                    {
                        showCageString = true;
                        if (Input.KeyboardHandler.WasKeyPressed(Keys.R) && isRight)
                        {
                            cageModel();
                            isRight = false;
                        }
                        else if (Input.KeyboardHandler.WasKeyPressed(Keys.L) && !isRight)
                        {
                            cageModel();
                            isRight = true;
                            if (bonesLeft == 0)
                            {
                                // now we set monster out side the cage, and need to run away!
                                monster.outOfCage = true;
                                monster.Position = new Vector3(0, 0, 35);
                                Random rnd = new Random();
                                int _row, _col;
                                do
                                {
                                    _row = rnd.Next(grid.rows - 4, grid.rows - 1);
                                    _col = rnd.Next(grid.columns - 4, grid.columns - 1);
                                }
                                while (grid.arr[_row, _col] == -1);
                                monsterSetDest(grid.r_cToPosition(_row - 1, _col - 1));
                            }
                        }
                    }
                    else
                        showCageString = false;
                }

                // update camera
                camera.Update(gameTime);
                // creat bounding sphere to the updated camera position
                cameraBS = new BoundingSphere(camera.calculatedPosition, 10f);
                camera.canWalk = true;
                if (CameraBoundries()) // if the player can walk camera position will get update   
                    camera.CameraPosition = camera.calculatedPosition; //can move

                camera.stepHeight += 0.01f;

                // create the new look - the updated or old camera position
                camera.CreateLookAt();

            } // End of play mode

            base.Update(gameTime);
        }


        // draw every frame of this state
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkRed);// red background
            // calls draw functions                                    
            groundDraw();
            cageDraw();
            grid.drawSquers();
            monster.Draw(gameTime);
            //draw the bone thrown at monster
            if (playerAttack)
            {
                BasicEffect effectToUpdate;
                effectToUpdate = throwBone.effect;

                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                throwBone.model.Draw(throwBone.Transform, camera.View, camera.Projection);
            }
            spriteBatch.Begin(); // start spite batch to print on screen

            if (showCageString && !monster.canMove)
                spriteBatch.DrawString(announceFont, "To remove the bones from the cage\nPress 'R' to try and break the right bone\nPress 'L' to try and break the left bone", new Vector2(25, 25), Color.White);
            if (monster.canMove && !playerAttack)
            {
                if (showMstr)
                    spriteBatch.DrawString(announceFont, "You can move this bone if you hit 'M'\nbut you should stay away after", new Vector2(25, 200), Color.Beige);
                if (showAstr && !monster.attacked)
                    spriteBatch.DrawString(announceFont, "You can hit it now!\nQuick, hit 'A' to attack the monster!", new Vector2(25, 25), Color.White);

            }
            //spriteBatch.DrawString(OurGame.announceFont, monster2.position_dest_STR(), new Vector2(25, 200), Color.White); // -> for tests

            // close spritebatch draw
            spriteBatch.End();

            base.Draw(gameTime);
        }

        // this function draws the ground model
        public void groundDraw()
        {
            BasicEffect effectToUpdate;
            effectToUpdate = ground.effect;

            effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
            effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
            effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
            effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
            effectToUpdate.DirectionalLight0.Enabled = true;
            effectToUpdate.TextureEnabled = true;
            effectToUpdate.LightingEnabled = true;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
            ground.model.Draw(ground.Transform, camera.View, camera.Projection);
        }
        // this function draws the cage model
        public void cageDraw()
        {
            BasicEffect effectToUpdate;
            effectToUpdate = cage.effect;

            effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
            effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
            effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
            effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
            effectToUpdate.DirectionalLight0.Enabled = true;
            effectToUpdate.TextureEnabled = true;
            effectToUpdate.LightingEnabled = true;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
            cage.model.Draw(cage.Transform, camera.View, camera.Projection);
        }

        // checks states status
        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);
            // Change to visible if not at the top of the stack
            // This way, sub menus will appear on top of this menu
            if (StateManager.State != this.Value)
            {
                Visible = true;
                monster.paused = true;
            }
            else
                monster.paused = false;
            // Sound Managing
            if (StateManager.State == this.Value && OurGame.EnableMusic)
                soundManager.ResumeSong();
            else
                soundManager.PauseSong();

            monster.playSound = OurGame.EnableSoundFx;
            this.playSound = OurGame.EnableSoundFx;
        }

        // Build Bounding Box
        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }


    }
}
