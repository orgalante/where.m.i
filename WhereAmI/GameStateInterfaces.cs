﻿using System;
using Microsoft.Xna.Framework;

using XELibrary;

namespace WhereAmI
{
    public interface IStartMenuState : IGameState { }
    public interface IChapter1State : IGameState { }
    public interface IChapter1HallwayState : IGameState { }
    public interface IChapter2State : IGameState { }
    public interface IChapter3State : IGameState { }
    public interface IDistruptionMenuState : IGameState { }
    public interface IOptionsMenuState : IGameState { }
    public interface IFinishStageMenuState : IGameState { }
    public interface IPlayingState : IGameState { }
    public interface IPausedState : IGameState { }

}
