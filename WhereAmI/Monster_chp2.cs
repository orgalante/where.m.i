using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using XELibrary;


namespace WhereAmI
{
    /// Monster_chp2 is a drawable class that controls every aspect of the "monster" in the games second level.
    /// it has its informative parameters such as his location, the player location and so on...
    /// the class determines the monster model patrol path in world by grid and to selected destination.
    /// the class determines the monster behavior when reaching too close to the player or bone.
    /// the class determines sounds such as attack sound.
    /// the monster can "attack" the player by coming to it and when touching it the game is over.

    public class Monster_chp2 : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public double lastAttackSound = 0;
        public Camera_2 Camera { get; set; }
        public Matrix World { get; set; }

        public Vector3 Position { get; set; }
        public Matrix Transform { get; set; } = Matrix.Identity;
        public Matrix Translation { get; set; } = Matrix.Identity;
        public Vector3 destination { get; set; }
        public bool playSound = true;

        public Vector3 playerPosition = new Vector3();
        public float disFromPlayer;
        public float disFromDst;

        public Model monsterModel;
        public BasicEffect monsterEffect;
        public BoundingBox monsterBB;
        public RasterizerState wireframeRaster, solidRaster;
        ISoundManager soundManager;
        public bool attacked = false;

        private double secFromStart = 0;
        public bool paused = false;

        public Grid grid { get; set; }
        string currPath;

        public int[] SrcSqr { get; private set; }
        public bool canMove = false;
        public bool IsNewPath = false;
        public int[,] ts;
        Vector3 currDstVec;
        public int wR, wC;
        public int[] DstSqr { get; private set; }
        public string[] chrrr { get; private set; }
        public float angle { get; private set; }
        float north = 90, west = 180, south = 270, east = 0;
        public enum direction { rowChange, colChange };
        direction pathDir { get; set; }
        bool IsUpLeft = false, IsDownLeft = false, IsUpRight = false, IsDownRight = false;
        int dropAngle = 1;
        int hitNum = 0;
        public bool gotHit = false;
        public bool outOfCage = false;
        public bool IsDead = false;
        // MONSTER_CHAPTER2 STATE
        public Monster_chp2(Game game)
            : base(game)
        {
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;

            initializeValues();
        }

        /// this method is a recursive method that go over a grid array backwards.
        /// it fills arr array with values according to the ways it chose.
        public void choosePath1(int n, int m)
        {
            // if we are standing on some 0 level we continue on the n=0/m=0 level
            if (n == wR || m == wC)
            {
                if (n == wR && m == wC) // destination cell
                    ts[wR, wC] = grid.arr[wR, wC];
                else if (n == wR) // we change only column
                {
                    if (grid.arr[n, m - 1] == -1) 
                        ts[n, m - 1] = Int32.MaxValue;
                    else
                    {
                        choosePath1(n, m - 1);
                        ts[n, m] = (ts[n, m - 1] != Int32.MaxValue ? grid.arr[n, m] + ts[n, m - 1] : Int32.MaxValue);
                    }
                }
                else// we change only row
                {
                    if (grid.arr[n - 1, m] == -1)
                        ts[n - 1, m] = Int32.MaxValue;
                    else
                    {
                        choosePath1(n - 1, m);
                        ts[n, m] = (ts[n - 1, m] != Int32.MaxValue ? grid.arr[n, m] + ts[n - 1, m] : Int32.MaxValue);
                    }
                }
            }
            // if neighbour is -1
            else if (grid.arr[n, m - 1] == -1 || grid.arr[n - 1, m] == -1) // if it is blocked
            {
                if (grid.arr[n, m - 1] == grid.arr[n - 1, m])
                    ts[n, m] = Int32.MaxValue;
                else if (grid.arr[n, m - 1] == -1) // row is blocked
                {
                    ts[n, m - 1] = Int32.MaxValue;
                    choosePath1(n - 1, m);
                    ts[n, m] = (ts[n - 1, m] != Int32.MaxValue ? grid.arr[n, m] + ts[n - 1, m] : Int32.MaxValue);
                }
                else // col is blocked
                {
                    ts[n - 1, m] = Int32.MaxValue;
                    choosePath1(n, m - 1);
                    ts[n, m] = (ts[n, m - 1] != Int32.MaxValue ? grid.arr[n, m] + ts[n, m - 1] : Int32.MaxValue);
                }
            }
            // else we continue to search path
            else if (grid.arr[n, m] == 2 || grid.arr[n, m] > ts[n, m])
            {
                choosePath1(n - 1, m);
                choosePath1(n, m - 1);
                if (ts[n - 1, m] != Int32.MaxValue || ts[n, m - 1] != Int32.MaxValue)
                    ts[n, m] = grid.arr[n, m] + Math.Min(ts[n - 1, m], ts[n, m - 1]);
                else
                    ts[n, m] = Int32.MaxValue;
            }

        }

        /// this method is a recursive method that go over a grid array backwards.
        /// it fills arr array with values according to the ways it chose.
        public void choosePath2(int n, int m)
        {
            // if we are standing on some 0 level we continue on the n=wantedR/m=wantedC level
            if (n == wR || m == wC)
            {
                if (n == wR && m == wC) // destination cell
                    ts[wR, wC] = grid.arr[wR, wC];
                else if (n == wR) // we change only column
                {
                    if (grid.arr[n, m - 1] == -1)
                        ts[n, m - 1] = Int32.MaxValue;
                    else
                    {
                        choosePath2(n, m - 1);
                        ts[n, m] = (ts[n, m - 1] != Int32.MaxValue ? grid.arr[n, m] + ts[n, m - 1] : Int32.MaxValue);
                    }
                }
                else // we change only row
                {
                    if (grid.arr[n + 1, m] == -1)
                        ts[n + 1, m] = Int32.MaxValue;
                    else
                    {
                        choosePath2(n + 1, m);
                        ts[n, m] = (ts[n + 1, m] != Int32.MaxValue ? grid.arr[n, m] + ts[n + 1, m] : Int32.MaxValue);
                    }
                }
            }
            // if neighbour is -1
            else if (grid.arr[n, m - 1] == -1 && grid.arr[n + 1, m] == -1) //if it is blocked
                ts[n, m] = Int32.MaxValue;
            else if (grid.arr[n, m - 1] == -1)
            {
                ts[n, m - 1] = Int32.MaxValue;
                choosePath2(n + 1, m);
                ts[n, m] = (ts[n + 1, m] != Int32.MaxValue ? grid.arr[n, m] + ts[n + 1, m] : Int32.MaxValue);
            }
            else if (grid.arr[n + 1, m] == -1)
            {
                ts[n + 1, m] = Int32.MaxValue;
                choosePath2(n, m - 1);
                ts[n, m] = (ts[n, m - 1] != Int32.MaxValue ? grid.arr[n, m] + ts[n, m - 1] : Int32.MaxValue);
            }
            // else we continue to search path
            else if (grid.arr[n, m] > ts[n, m])
            {
                choosePath2(n + 1, m);
                choosePath2(n, m - 1);
                if (ts[n + 1, m] != Int32.MaxValue || ts[n, m - 1] != Int32.MaxValue)
                    ts[n, m] = grid.arr[n, m] + Math.Min(ts[n + 1, m], ts[n, m - 1]);
                else
                    ts[n, m] = Int32.MaxValue;
            }
        }

        /// this method is a recursive method that go over a grid array backwards.
        /// it reads arr recursively and translate the path into a string array
        public string getPath(int n, int m)
        {
            if (n == wR || m == wC)
            {
                if (n == wR && m == wC)
                    return n + "," + m;
                else return (m > wC ? getPath(n, m - 1) : getPath(n - 1, m)) + "," + n + "," + m;
            }
            else
                return (ts[n - 1, m] < ts[n, m - 1] ? getPath(n - 1, m) : getPath(n, m - 1)) + "," + n + "," + m;
        }

        /// this method is a recursive method that go over a grid array backwards.
        /// it reads arr recursively and translate the path into a string array
        public string getPath2(int n, int m)
        {
            if (n == wR || m == wC)
            {
                if (n == wR && m == wC) return n + "," + m;
                else return (m > wC ? getPath2(n, m - 1) : getPath2(n + 1, m)) + "," + n + "," + m;
            }
            else
                return (ts[n + 1, m] < ts[n, m - 1] ? getPath2(n + 1, m) : getPath2(n, m - 1)) + "," + n + "," + m;
        }

        public override void Initialize()
        {
            initializeValues();
            base.Initialize();
        }
        // this method reset the monster parameters to its initial values
        void initializeValues()
        {
            secFromStart = 0;
            attacked = false;
            angle = 1;
            DstSqr = new int[] { -1, -1 };
            dropAngle = 0;
        }
        // this method load monster contents 
        protected override void LoadContent()
        {
            monsterModel = Game.Content.Load<Model>(@"Models\bone1");
            monsterEffect = monsterModel.Meshes[0].MeshParts[0].Effect as BasicEffect;
            Transform = Matrix.CreateScale(Vector3.One);
            base.LoadContent();
        }

        // this function calculates a new path and destination for monster on the grid
        public void calculateNewPath()
        {
            /// illustrate grid:(-1 = lock),(100 = free),(-2 = dst)
            /// -1 |100|100| 2
            /// 100|100|-1 |100
            /// 100|100|-1 |100
            if (DstSqr[0] != -1) // return old dst to -1 value of grid
                grid.arr[DstSqr[0], DstSqr[1]] = -1;
            DstSqr = grid.positionToR_C(destination);
            grid.arr[DstSqr[0], DstSqr[1]] = 2; // make new dst to be 2
            ts = new int[grid.rows + 1, grid.columns + 1];
            ///            |
            ///   IsUpLeft | IsUpRight
            /// -------------------------
            /// IsDownLeft | IsDownRight
            ///            |
            IsUpLeft = (SrcSqr[0] > DstSqr[0]) && (SrcSqr[1] > DstSqr[1]); // n>R ,m>C
            IsDownLeft = (SrcSqr[0] < DstSqr[0]) && (SrcSqr[1] > DstSqr[1]); // n<R ,m>C
            IsUpRight = (SrcSqr[0] > DstSqr[0]) && (SrcSqr[1] < DstSqr[1]); // n>R ,m<C
            IsDownRight = (SrcSqr[0] < DstSqr[0]) && (SrcSqr[1] < DstSqr[1]); // n<R ,m<C
            // checks if destination smaller than source position on the grid, on that base- sends to the relevant function
            if (IsDownLeft || IsUpRight)
            {
                if (IsDownLeft)
                {
                    wR = DstSqr[0]; wC = DstSqr[1];
                    choosePath2(SrcSqr[0], SrcSqr[1]);
                    currPath = getPath2(SrcSqr[0], SrcSqr[1]);
                }
                else
                {
                    wR = SrcSqr[0]; wC = SrcSqr[1];
                    choosePath2(DstSqr[0], DstSqr[1]);
                    currPath = getPath2(DstSqr[0], DstSqr[1]);
                }
            }
            else // if (IsUpLeft||IsDownRight||same level
            {
                if (IsUpLeft)
                {
                    wR = DstSqr[0]; wC = DstSqr[1];
                    choosePath1(SrcSqr[0], SrcSqr[1]);
                    currPath = getPath(SrcSqr[0], SrcSqr[1]);
                }
                else
                {
                    wR = Math.Min(SrcSqr[0], DstSqr[0]);
                    wC = Math.Min(SrcSqr[1], DstSqr[1]);
                    int n = Math.Max(SrcSqr[0], DstSqr[0]);
                    int m = Math.Max(SrcSqr[1], DstSqr[1]);
                    choosePath1(n, m);
                    currPath = getPath(n, m);
                }
            }
            chrrr = currPath.Split(',');
            if (chrrr.Length > 2)
                initDst();
        }

        // this function initialize the current destination of monster
        public void initDst()
        {
            string[] temp = chrrr;
            int i = -1;
            if (IsDownRight || IsUpRight) // reading path dirction --->
            {
                i = 0; // from start
                if (chrrr[i] != chrrr[i + 2]) pathDir = direction.rowChange;
                else if (chrrr[i + 1] != chrrr[i + 3]) pathDir = direction.colChange;
                // checks where in the path the direction changes
                while (i < chrrr.Length - 2 && ((((chrrr[i] != chrrr[i + 2]) && pathDir == direction.rowChange) || ((chrrr[i + 1] != chrrr[i + 3]) && pathDir == direction.colChange))))
                    i += 2;
                chrrr = new string[temp.Length - i];
                Array.Copy(temp, i, chrrr, 0, chrrr.Length);
                currDstVec = grid.r_cToPosition(Int32.Parse(chrrr[0]), Int32.Parse(chrrr[1]));
                DstSqr = new int[] { Int32.Parse(chrrr[0]), Int32.Parse(chrrr[1]) };
            }
            else // reading path dirction <----
            {
                i = chrrr.Length - 1; // from end
                if (chrrr[i - 1] != chrrr[i - 3]) pathDir = direction.rowChange;
                else if (chrrr[i] != chrrr[i - 2]) pathDir = direction.colChange;
                // checks where in the path the direction changes
                while (i > 2 && (((chrrr[i - 1] != chrrr[i - 3]) && pathDir == direction.rowChange) || ((chrrr[i] != chrrr[i - 2]) && pathDir == direction.colChange)))
                    i -= 2;
                chrrr = new string[i + 1];
                Array.Copy(temp, 0, chrrr, 0, chrrr.Length);
                currDstVec = grid.r_cToPosition(Int32.Parse(chrrr[i - 1]), Int32.Parse(chrrr[i]));
                DstSqr = new int[] { Int32.Parse(chrrr[i - 1]), Int32.Parse(chrrr[i]) };
            }
            canMove = true;
            if (DstSqr[0] > SrcSqr[0] + 1) // monster moves north
                angle = north;
            else if (DstSqr[0] < SrcSqr[0] - 1) // monster moves south
                angle = south;
            else if (DstSqr[1] > SrcSqr[1] + 1) // monster moves east
                angle = east;
            else // monster moves west
                angle = west;
            setNextStep();
        }

        // UPDATE every frame this monster parameters
        public override void Update(GameTime gameTime)
        {
            disFromDst = Vector3.Distance(currDstVec, this.Position);
            disFromPlayer = (float)Vector3.Distance(playerPosition, this.Position);

            // play mode
            if (!paused)
            {
                // case the monster got hit by player
                if (gotHit && hitNum < 4)
                {
                    if (dropAngle < 90) // determin the falling angle on monster
                        dropAngle++;
                    else if (hitNum < 3) // change monster model according to hit count
                    {
                        hitNum++;
                        monsterModel = Game.Content.Load<Model>(@"Models\bone1_Hit" + hitNum);
                        gotHit = false;
                        dropAngle = 1;
                    }
                    else
                        IsDead = true; // if got hit for the 4th time- dead
                }
                // case monster stil play and out of cage
                else if (canMove)
                {
                    SrcSqr = grid.positionToR_C(Position);
                    if (IsNewPath) 
                    {
                        calculateNewPath();
                        IsNewPath = false;
                    }
                    else if (disFromDst < 15)
                    {
                        if (chrrr.Length > 2)
                            initDst();
                        else  // if monster have no new destination -she run after player to attack it.
                        {
                            attacked = true;
                            IsNewPath = true;
                            destination = Camera.CameraPosition;
                        }
                    }
                    else if (attacked) 
                    {
                        // play bone moving sound
                        secFromStart = gameTime.TotalGameTime.TotalSeconds;
                        float volume = (20 / disFromPlayer);
                        if (secFromStart - lastAttackSound > 3 && playSound && volume <= 1)
                        {
                            lastAttackSound = secFromStart;
                            soundManager.Play("Attack-07", volume); // plays Attack sound
                        }
                        setNextStep();
                    }
                    else
                        setNextStep();
                }
            }
            base.Update(gameTime);
        }

        // draw monster model in world
        public override void Draw(GameTime gameTime)
        {
            // if got hit draw monster falling down
            if (gotHit)
            {                // drop body 
                Translation =
                  Matrix.CreateRotationX(MathHelper.ToRadians(dropAngle * (-1))) *
                (Matrix.CreateTranslation(Position + new Vector3(0, 3, 0)));
            }
            GraphicsDevice.RasterizerState = solidRaster;

            BasicEffect effectToUpdate;
            effectToUpdate = monsterEffect;
            effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
            effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
            effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
            effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
            effectToUpdate.DirectionalLight0.Enabled = true;
            effectToUpdate.TextureEnabled = true;
            effectToUpdate.LightingEnabled = true;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
            monsterModel.Draw(Translation, Camera.View, Camera.Projection);
        }
        // test string function
        public override string ToString()
        {
            string str;
            str = "Monster:\nPosition: " + (int)Position.X + "," + (int)Position.Y + "," + (int)Position.Z + "\n";
            str += "P_position: " + (int)playerPosition.X + "," + (int)playerPosition.Y + "," + (int)playerPosition.Z + "\n";
            str += "distance_P: " + (int)disFromPlayer + "\n";
            str += "sec: " + secFromStart + "\n";
            str += "lastAttack: " + lastAttackSound;
            return str;
        }
        // sets the monster next position based on all game changing parameters such as angle, destination and so
        public void setNextStep()
        {
            float speed = 1.5f;
            float elapsed = 0.1f;
            Vector3 direction = Vector3.Normalize(currDstVec - Position);
            Position += direction * elapsed * speed;
            Translation = Matrix.CreateRotationY((float)(Math.PI / 180) * angle) * Matrix.CreateTranslation(Position) * World;

        }

        // this function builds bounding box for model
        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }
        // test string function
        public string position_dest_STR()
        {
            string str = "monster Vector: " + (int)Position.X + "," + (int)Position.Y + "," + (int)Position.Z + "\n";
            str += "currDstVec: " + (int)currDstVec.X + "," + (int)currDstVec.Y + "," + (int)currDstVec.Z + "\n";
            int[] result = grid.positionToR_C(Position);
            int[] result2 = grid.positionToR_C(destination);
            str += "monster-arr[ " + result[0] + "," + result[1] + "] , value: " + grid.arr[result[0], result[1]] + "\n";
            str += "Dest-arr[ " + result2[0] + "," + result2[1] + "] , value: " + grid.arr[result2[0], result2[1]] + "\n";

            return str;
        }
    }
}
