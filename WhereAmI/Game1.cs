using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XELibrary;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WhereAmI
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        
        GraphicsDeviceManager graphics;

        InputHandler input;
        public SpriteFont announceFont;
        public SpriteBatch spriteBatch;
        public SoundManager soundManager;

        public IStartMenuState StartMenuState; //state of start menu of game.
        public IChapter1State Chapter1State; //state of Chapter 1 of game.
        public IChapter1HallwayState Chapter1HallwayState; //state of Chapter 1 hallway of game.
        public IChapter2State Chapter2State; //state of Chapter 2 of game.
        public IChapter3State Chapter3State; //state of Chapter 3 of game.
        public IDistruptionMenuState DistruptionMenuState;
        public IOptionsMenuState OptionsMenuState;
        public IFinishStageMenuState FinishStageMenuState;
        public IPlayingState PlayingState;
        public IPausedState PausedState;
        GameStateManager stateManager;
        public int currPlayStateNum = 0;

        public class ModelClass
        {
            public Model model { get; set; }
            public Matrix Transform { get; set; }
            public BasicEffect effect { get; set; }
            public BoundingBox BB { get; set; }
            public Vector3 position { get; set; }
            public bool action = false;
            public int angle = 0;
        }

        public class BoundingBoxBuffers
        {
            public VertexBuffer Vertices;
            public int VertexCount;
            public IndexBuffer Indices;
            public int PrimitiveCount;
        }

        // to use songs in game.
        public bool EnableSoundFx { get; set; }
        public bool EnableMusic { get; set; }

        public Game1()
        {

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            //set the GraphicsDeviceManager's fullscreen property
            //graphics.IsFullScreen = true;


            //graphics.PreferredBackBufferWidth = 1900;
            //graphics.PreferredBackBufferHeight = 1000;
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 768;



            input = new InputHandler(this);
            Components.Add(input);

            EnableSoundFx = true;
            EnableMusic = true;

            soundManager = new SoundManager(this);
            Components.Add(soundManager);

            stateManager = new GameStateManager(this);
            Components.Add(stateManager);

            StartMenuState = new StartMenuState(this);
            Chapter1State = new Chapter1State(this);
            Chapter1HallwayState = new Chapter1HallwayState(this);
           Chapter2State = new Chapter2State(this);
            Chapter3State = new Chapter3State(this);

            DistruptionMenuState = new DistruptionMenuState(this);
            OptionsMenuState = new OptionsMenuState(this);
            FinishStageMenuState = new FinishStageMenuState(this);
            // PlayingState = new PlayingState(this);
            PausedState = new PausedState(this);

            

        }

        protected override void BeginRun()
        {
            stateManager.ChangeState(StartMenuState.Value);

            base.BeginRun();
        }


        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
            announceFont = Content.Load<SpriteFont>(@"Fonts\AnnouncementFont");

            // Load sounds
            string musicPath = @"Music\Songs";
            string fxPath = @"Music\Sounds";
            soundManager.LoadContent(musicPath, fxPath);

            base.LoadContent();
        }


        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
    

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {



            base.Draw(gameTime);


        }

  



    }
}
