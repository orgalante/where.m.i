﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    /// Chapter1State is the state that controls every aspect of the first level in the game.
    /// it has a cave object as the level world, a monster object to hide from and a camera object to draw the level from first person view.
    /// the state controls actions between his objects, the level game play and moving to other states if has to or finished successfully.
    /// in order to pass this level the player has to find a syringe in the cave, then try to grab it, and then find the vein in the 
    /// cave and try to get to it and stick the syringe in it. if the player succeedes in doing that without getting caught by the monster
    /// he passes that level.

    public sealed class Chapter3State : BaseGameState, IChapter3State
    {
        // chapter1 parameters 3d
        public Camera_2 camera; //
        public SpriteBatch spriteBatch;
        private SpriteFont announceFont;
        Matrix world;
        BoundingSphere cameraBS;
        Player_chp1 player3;
        Monster_chp3 monster3;
        ISoundManager soundManager;

        bool canMove = true;
        bool finishedState = false;
        private bool playSound;
        string[][] mapStr;
        Grid grid { get; set; }

        public List<Game1.ModelClass> staticModelsListS3 = new List<Game1.ModelClass>(10);
        public Vector3 camPositionSaver;
        public Camera_2 camera_backup;
        public bool camUpMode = false;

        // CHAPTER3 STATE
        public Chapter3State(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IChapter3State), this); //add this state to game service.
            // Sound
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
            // Camera
            Vector3 camPosition = new Vector3(0, 13, 0); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-25, 13, -25); // position of target to look at in world
            camera = new Camera_2(game, camPosition, camLookTo); // camera first look initialize
            // World 
            world = Matrix.Identity;
            //  grid build
            mapStr = loadCsvFile("chapter3map.csv");
            grid = new Grid(Game, mapStr.Length, mapStr[0].Length);
            OurGame.Components.Add(grid);
            // player3 build
            player3 = new Player_chp1(game);
            OurGame.Components.Add(player3);
            player3.Camera = camera;
            player3.World = world;

            // monster3 build
            monster3 = new Monster_chp3(game);
            OurGame.Components.Add(monster3);
            monster3.Position = new Vector3(16, 0, 16);
            monster3.Camera = camera;
            monster3.World = world;
            monster3.playerPosition = player3.Position;

        }

        /// this method reset the state parameters to its initial values.
        public override void Initialize()
        {
            canMove = true;

            // Camera
            camera.Dispose();
            Vector3 camPosition = new Vector3(0f, 13, 0f); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-25, 13, -25); // position of target to look at in world
            camera = new Camera_2(OurGame, camPosition, camLookTo); // camera first initialize

            // player3 build
            player3.Camera = camera;
            player3.Position = camera.CameraPosition;

            // monster3 build
            monster3.Position = new Vector3(16, 0, 16);
            monster3.Camera = camera;
            monster3.playerPosition = player3.Position;


            base.Initialize();
        }

        /// this method load contents such as models, fonts and sounds and conects them to this state parameters
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // font is load for printing on screen for player or for debug.
            announceFont = Game.Content.Load<SpriteFont>(@"Fonts\StartMenuFont");

            camera.CreateLookAt(); // creates first look
 
            addModel("floor1", Vector3.Zero, Vector3.One);

            csvStrMapToModels(); // reads the state map and translate it to grid
            csvStrToModels(loadCsvFile("chapter3.csv")); // reads some static models settings

            base.LoadContent();
        }

        // read csv file function
        public string[][] loadCsvFile(string file_name)
        {
            // Read each line of the file into a string array. 
            var csvFilePath = Directory.GetCurrentDirectory() + "\\Content\\Files\\" + file_name;
            return File.ReadLines(csvFilePath).Where(line => line != "").Select(x => x.Split(',')).ToArray();
            //// Read each line of the file into a string array.
            //var csvFilePath = Directory.GetCurrentDirectory() + "\\" + file_name;
            //return File.ReadLines(csvFilePath).Where(line => line != "").Select(x => x.Split(',')).ToArray();
        }


        public void csvStrToModels(string[][] str)
        {
            // we start from row 1 since 0 is headline
            for (int r = 1; r < str.Length; r++)  //for each row
            {
                float[] pArr = new float[6];
                for (int c = 0; c < 6; c++) //for each column
                    pArr[c] = (float)Convert.ToDouble(str[r][c]);
                Vector3 position = new Vector3(pArr[0], pArr[1], pArr[2]);
                Vector3 scale = new Vector3(pArr[3], pArr[4], pArr[5]);
                addModel(str[r][6], position, scale);
            }
        }

        public void csvStrMapToModels()
        {
            grid.Camera = camera;
            grid.World = world;
            for (int r = 0; r < grid.rows; r++) //for each row
                for (int c = 0; c < grid.columns; c++) //for each column
                    if (mapStr[r][c] != "0") addModel(mapStr[r][c], grid.r_cToPosition(r, c), new Vector3(1, 1, 1));
        }

        public void addModel(string modelName, Vector3 pos, Vector3 scale)
        {
            staticModelsListS3.Add(new Game1.ModelClass());
            staticModelsListS3[staticModelsListS3.Count() - 1].model = Game.Content.Load<Model>(@"Models\" + modelName);
            staticModelsListS3[staticModelsListS3.Count() - 1].effect = staticModelsListS3[staticModelsListS3.Count() - 1].model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            staticModelsListS3[staticModelsListS3.Count() - 1].position = pos;
            staticModelsListS3[staticModelsListS3.Count() - 1].Transform = Matrix.CreateTranslation(staticModelsListS3[staticModelsListS3.Count() - 1].position) * Matrix.CreateScale(scale) * world;
            staticModelsListS3[staticModelsListS3.Count() - 1].BB = BuildBoundingBox(staticModelsListS3[staticModelsListS3.Count() - 1].model, staticModelsListS3[staticModelsListS3.Count() - 1].Transform);
            grid.addWallModel(staticModelsListS3[staticModelsListS3.Count() - 1]);
        }

        /// <summary>
        /// update every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {

            //if finished the level successfully ->end level
            if (finishedState)
            {
                monster3.paused = true; //so the monster will stop moving
                                        // moving the user to the Finish Stage Menu State
                soundManager.StopPlayList();
                StateManager.ChangeState(OurGame.FinishStageMenuState.Value);
            }

            //// if monster model touches the player-> game ended
            //if (monster3.attacked)
            //{
            //    soundManager.StopPlayList();
            //    StateManager.PopState();
            //    StateManager.ChangeState(OurGame.StartMenuState.Value);
            //}

            // if player press p -> game paused.
            if (Input.KeyboardHandler.WasKeyPressed(Keys.P))
            {
                // Push the user to the distruption menu State
                StateManager.PushState(OurGame.DistruptionMenuState.Value);
            }

            // creat bounding sphere to the updated camera position
            cameraBS = new BoundingSphere(camera.calculatedPosition, 1f);



            bool cullusion = false;
            camera.canWalk = true;
            // roundBS is the exit position of that circle in the cave.
            // if player is on circle function but is on one of the roundBS, he can walk
            //bool canPass = round2BS.Intersects(cameraBS) || round1BS.Intersects(cameraBS);
            foreach (Game1.ModelClass s in staticModelsListS3)
            {
                if (s.BB.Intersects(cameraBS))
                    cullusion = true;
            }

            if (Input.KeyboardHandler.WasKeyPressed(Keys.U) && !camUpMode)
            {
                camUpMode = true;
                camPositionSaver = camera.CameraPosition;
                Vector3 camPosition = new Vector3(0f, 13, 0f); // position of camera/player in world
                Vector3 camLookTo = new Vector3(-25, 13, -25); // position of target to look at in world
                camera_backup = new Camera_2(OurGame, camPosition, camLookTo); // camera first initialize
                camera = new Camera_2(OurGame, new Vector3(0f, 250, 0f), new Vector3(20, 10, -10)); // camera first initialize
                                                                                                    //canMove = false;
                camera.CreateLookAt();
            }


            if (Input.KeyboardHandler.WasKeyPressed(Keys.D) && camUpMode)
            {
                camUpMode = false;
                camera = camera_backup; // camera first initialize
                canMove = true;
                camera.CreateLookAt();
            }

            // update camera
            camera.Update(gameTime);

            if (canMove)
            {
                // if there is a collusion , player can't move so camera position won't get update
                if (!cullusion)
                {
                    // if the player can walk camera position will get update
                    camera.CameraPosition = camera.calculatedPosition; //can move
                    camera.stepHeight += 0.05f;
                }
                // create the new look we the updated or old camera position
                camera.CreateLookAt();
            }

            // player gets its updated position
            player3.Position = camera.CameraPosition;
            // monster gets player position
            monster3.playerPosition = camera.CameraPosition;
            monster3.Camera = camera;
            grid.Camera = camera;
            grid.World = world;




            base.Update(gameTime);
        }

        /// <summary>
        /// draw every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            monster3.Draw(gameTime);
            // start spite batch to print on screen
            spriteBatch.Begin();

            grid.drawSquers();

            // close spritebatch draw
            spriteBatch.End();

            base.Draw(gameTime);
        }




        // checks states status
        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);

            // Change to visible if not at the top of the stack
            // This way, sub menus will appear on top of this menu
            if (StateManager.State != this.Value)
            {
                Visible = true;

            }


            // Sound Managing
            if (StateManager.State == this.Value && OurGame.EnableMusic)
                soundManager.ResumeSong();
            else
                soundManager.PauseSong();


            this.playSound = OurGame.EnableSoundFx;
        }


        // Build Bounding Box
        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }



    }
}
