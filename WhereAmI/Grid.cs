using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using XELibrary;

namespace WhereAmI
{
    /// Cave is a drawable class that controls the cave area in the games first level.
    /// the class draws the cave model during the game.
    /// if the the player managed to stick the syringe in the vein (finish the state) 
    /// the cave class will draw its model with and without lightning for 20 seconds.
    public class Squer
    {
        public Vector2 center { get; set; }
        public bool taken { get; set; }
        public Game1.ModelClass model { get; set; }
    }


    public class Grid : Microsoft.Xna.Framework.DrawableGameComponent
    {

        public Camera_2 Camera { get; set; }
        public Matrix World { get; set; }


        public int columns, rows;
        public int[,] arr;
        public Squer[,] Array;
        int L = 10;

        public RasterizerState wireframeRaster, solidRaster;

        public List<Game1.ModelClass> staticModelsList = new List<Game1.ModelClass>();
        public List<Game1.ModelClass> bonesModelsList= new List<Game1.ModelClass>();
        public int limitZ, limitX;

        public Grid(Game game, int rs, int cs)
            : base(game)
        {
            rows = rs;
            columns = cs;
            arr = new int[rows, columns];
            Array = new Squer[rows, columns];
            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;

            int rangeZ = rows * L, rangeX = columns * L;
            limitZ = rangeZ / 2; limitX = rangeX / 2;

            // first = up-left in matrix, (-x,+z) on coordinate system
            int firstZ = -1 * limitZ, firstX = -1 * limitX;
            Vector2 firstCellCenter = new Vector2(firstX + (L / 2), firstZ + (L / 2));

            int centerZ = (int)firstCellCenter.Y;
            Squer s = new Squer();
            s.center = new Vector2(5, 3);
            //for each row
            int centerX = (int)firstCellCenter.X;
            for (int r = 0; r < rows; r++, centerX += 10)
            {
                //for each column
                for (int c = 0; c < columns; c++, centerZ += 10)
                {
                    Array[r, c] = new Squer();
                    Array[r, c].center = new Vector2(centerX, centerZ);
                    arr[r, c] = 100;
                }
                centerZ = (int)firstCellCenter.Y;
            }
        }

        public int[] positionToR_C(Vector3 pos)
        {
            int z, x, r, c;
            // calculates the nearest squer-center for pos
            z = pos.Z < 0 ? -1 * (-1 * ((int)(pos.Z / 10) * 10) + 5) : (int)((int)(pos.Z / 10) * 10 + 5);
            x = pos.X < 0 ? -1 * (-1 * ((int)(pos.X / 10) * 10) + 5) : (int)((int)(pos.X / 10) * 10 + 5);

            c = z < 0 ? (int)((z + ((columns / 2) * 10)) / 10) : columns - ((int)(((columns / 2) * 10 - z) / 10));
            r = x < 0 ? (int)((x + ((rows / 2) * 10)) / 10) : rows - ((int)(((rows / 2) * 10 - x) / 10));

            return new int[]{ r,c};
        }

        public Vector3 r_cToPosition(int r, int c)
        {
            int z = c < columns / 2 ? (c - (columns / 2)) * L + 5 : -1 * (((columns / 2) - c) * L - 5);
            int x = r < rows / 2 ? (r - (rows / 2)) * L + 5 : -1 * (((rows / 2) - r) * L - 5);
            //  int z = r < rows / 2 ? ((rows / 2) - r) * L - 5 : -1 * ((r - rows / 2) * L + 5);
            return new Vector3(x, 0, z);
        }

        public void addModel(Game1.ModelClass model)
        {
            staticModelsList.Add(model);
            int[] r_c = positionToR_C(model.position);

            Array[r_c[0], r_c[1]].taken = true;
            Array[r_c[0], r_c[1]].model = model;
            arr[r_c[0], r_c[1]] = -1;

            if (model.position == Vector3.Zero)
                fill_arr(tryFunc(model.model, model.Transform));
        }


        public void addWallModel(Game1.ModelClass model)
        {
            staticModelsList.Add(model);
            int[] r_c = positionToR_C(model.position);

            Array[r_c[0], r_c[1]].taken = true;
            Array[r_c[0], r_c[1]].model = model;
            arr[r_c[0], r_c[1]] = -1;

            if (model.position == Vector3.Zero)
                fill_arr(tryFunc(model.model, model.Transform));
        }



        public void addBoneModel(Game1.ModelClass model)
        {
            bonesModelsList.Add(model);
            int[] r_c = positionToR_C(model.position);

            Array[r_c[0], r_c[1]].taken = true;
            Array[r_c[0], r_c[1]].model = model;
            arr[r_c[0], r_c[1]] = -1;
        }

        public void fill_arr(int[][] min_max)
        {
            for (int r = min_max[0][0]; r < min_max[1][0]; r++)
                for (int c = min_max[0][1]; c < min_max[1][1]; c++)
                    arr[r, c] = -1;
        }

        public void removeModel()
        {

        }

        public int[][] tryFunc(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xz values for the mesh
            Vector2 meshMax = new Vector2(float.MinValue);
            Vector2 meshMin = new Vector2(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;
                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);
                    // Find minimum and maximum xz values for this mesh part
                    Vector2 vertPosition = new Vector2();
                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = new Vector2(vertexData[i].Position.X, vertexData[i].Position.Z);
                        // update our values from this vertex
                        meshMin = Vector2.Min(meshMin, vertPosition);
                        meshMax = Vector2.Max(meshMax, vertPosition);
                    }
                }
                // transform by mesh bone matrix
                meshMin = Vector2.Transform(meshMin, meshTransform);
                meshMax = Vector2.Transform(meshMax, meshTransform);
            }
            int[][] res = { positionToR_C(new Vector3(meshMin.X, 0, meshMin.Y)), positionToR_C(new Vector3(meshMax.X, 0, meshMax.Y)) };
            return res;
        }

        public void drawSquers()
        {

            foreach (Game1.ModelClass m in staticModelsList)
            {         
                BasicEffect effectToUpdate;
                effectToUpdate = m.effect;

                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                m.model.Draw(m.Transform, Camera.View, Camera.Projection);
            }

            foreach (Game1.ModelClass m in bonesModelsList)
            {
                BasicEffect effectToUpdate;
                effectToUpdate = m.effect;

                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                m.model.Draw(m.Transform, Camera.View, Camera.Projection);
            }



        }

        protected override void LoadContent()
        {

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {




        }

        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }




    }
}



