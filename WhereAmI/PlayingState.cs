﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace WhereAmI
{
    public enum DirectionType { Left, Right };

    public sealed class PlayingState : BaseGameState, IPlayingState
    {
        public static float G = 9.81f;

        private Vector2 position = new Vector2(20, 520);
        private Vector2 velocity = new Vector2(200.0f, 0.0f);
        private DirectionType direction = DirectionType.Right;
        private float mass = 70.0f;

        private string[] playList = { "Buoyant Iceberg" };

        IScrollingBackgroundManager scrollingBackgroundManager;

        ICelAnimationManager celAnimationManager;

        ISoundManager soundManager;

        public PlayingState(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IPlayingState), this);

            scrollingBackgroundManager = (IScrollingBackgroundManager)game.Services.GetService(typeof(IScrollingBackgroundManager));
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
        }

        protected override void LoadContent()
        {
            CelCount celCount = new CelCount(7, 1);
            celAnimationManager.AddAnimation("sonicRun", "sonic", celCount, 10,7,0,0);
           celAnimationManager.ToggleAnimation("sonicRun");

            scrollingBackgroundManager.AddBackground("snowBG", "snow_bg", new Vector2(0, 0), new Rectangle(0, 0, 1600, 600), 10, 1.0f, Color.White);
            scrollingBackgroundManager.AddBackground("caveWalk", "cave_walk", new Vector2(0, 550), new Rectangle(0, 0, 512, 60), 100, 0.0f, Color.White);
        }

        public override void Update(GameTime gameTime)
        {
            handleInput(gameTime);

            base.Update(gameTime);
        }

        private void handleInput(GameTime gameTime)
        {
            if (Input.KeyboardHandler.WasKeyPressed(Keys.Escape))
                StateManager.PushState(OurGame.DistruptionMenuState.Value);
            if (Input.KeyboardHandler.WasKeyPressed(Keys.P))
                StateManager.PushState(OurGame.PausedState.Value);


            if (Input.KeyboardHandler.IsKeyDown(Keys.Right) && position.X >= (Game.GraphicsDevice.Viewport.Width / 2.0f))
                scrollingBackgroundManager.ScrollRate = -2.0f;
            else if (Input.KeyboardHandler.IsKeyDown(Keys.Left) && position.X <= 0.0f)
                scrollingBackgroundManager.ScrollRate = 2.0f;
            else
                scrollingBackgroundManager.ScrollRate = 0.0f;

            if (Input.KeyboardHandler.IsKeyDown(Keys.Right))
            {
                celAnimationManager.ResumeAnimation("sonicRun");
                direction = DirectionType.Right;
                position.X += (int)(velocity.X * gameTime.ElapsedGameTime.TotalSeconds);
            }
            else if (Input.KeyboardHandler.IsKeyDown(Keys.Left))
            {
                celAnimationManager.ResumeAnimation("sonicRun");
                direction = DirectionType.Left;
                position.X -= (int)(velocity.X * gameTime.ElapsedGameTime.TotalSeconds);
            }
            else
                celAnimationManager.PauseAnimation("sonicRun");

            if (position.X > (GraphicsDevice.Viewport.Width / 2.0f))
                position.X = (GraphicsDevice.Viewport.Width / 2.0f);
            if (position.X < 0)
                position.X = 0;

            if (Input.KeyboardHandler.WasKeyPressed(Keys.Space) && position.Y == 520)
            {
                if (OurGame.EnableSoundFx)
                    soundManager.Play("Jump");
                velocity.Y = -300.0f;
            }

            position.Y += (int)(velocity.Y * gameTime.ElapsedGameTime.TotalSeconds);

            if (position.Y > 520)
            {
                position.Y = 520;
                velocity.Y = 0;
            }
            if (position.Y < 520)
                velocity.Y += (int)(G * mass * gameTime.ElapsedGameTime.TotalSeconds);
        }

        public override void Draw(GameTime gameTime)
        {
            scrollingBackgroundManager.Draw("snowBG", OurGame.spriteBatch);
            scrollingBackgroundManager.Draw("caveWalk", OurGame.spriteBatch);
            celAnimationManager.Draw(gameTime, "sonicRun", OurGame.spriteBatch, position, direction == DirectionType.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);

            base.Draw(gameTime);
        }

        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);

            // Change to visible if not at the top of the stack
            if (StateManager.State != this.Value)
            {
                Visible = true;
                soundManager.StopPlayList();
            }
            else
            {
                if (OurGame.EnableMusic)
                    soundManager.StartPlayList(playList);
            }
        }

    }
}
