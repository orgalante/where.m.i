﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    /// Chapter1HallwayState is the state that ....

    public sealed class Chapter1HallwayState : BaseGameState, IChapter1HallwayState
    {
        // chapter1 parameters 3d
        public Camera_2 camera; //
        public SpriteBatch spriteBatch;
        private SpriteFont announceFont;
        Cave cave;
        Player_chp1 player1;

        Game1.ModelClass Hallway = new Game1.ModelClass();

        Matrix world;

        BoundingSphere cameraBS;

        ISoundManager soundManager;

        bool canMove = false;
        private double endTime = 0;
        bool finishedState = false;
        private bool playMusic;
        private bool playSound;
        public RasterizerState wireframeRaster, solidRaster;

        private bool stopChecking = false;

        // CHAPTER1 STATE
        public Chapter1HallwayState(Game game)
            : base(game)
        {

            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;


            game.Services.AddService(typeof(IChapter1HallwayState), this); //add this state to game service.

            // Sound
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));

            // Camera
            Vector3 camPosition = new Vector3(0f, 50f, 0f); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-30, 50, -195); // position of target to look at in world
            camera = new Camera_2(game, camPosition, camLookTo); // camera first look initialize

            // World 
            world = Matrix.Identity; // Scale everything to a smaller size

            // Player1 build
            player1 = new Player_chp1(game);
            OurGame.Components.Add(player1);
            player1.Camera = camera;
            player1.World = world;

            //Hallway build


            // Cave build
            cave = new Cave(game);
            OurGame.Components.Add(cave);
            cave.Camera = camera;
            cave.World = world;

            // add Bandage
            Bandage bandage = new Bandage(game);

        }

        /// <summary>
        /// this method reset the state parameters to its initial values.
        /// </summary>
        public override void Initialize()
        {
            stopChecking = false;

            canMove = false;
            endTime = 0;

            //playing the first instruction voice over
            string[] playList = { "Voice 005", "Voice 006" };
            //soundManager.StartPlayList(playList, 0);
            //soundManager.RepeatPlayList = false;

            // Camera
            camera.Dispose();
            Vector3 camPosition = new Vector3(0f, 5f, 0f); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-15, 5, -15); // position of target to look at in world
            camera = new Camera_2(OurGame, camPosition, camLookTo); // camera first initialize

            // Player1 build
            player1.Camera = camera;
            player1.Position = camera.CameraPosition;



            // Cave build
            cave.Camera = camera;

            base.Initialize();
        }

        /// <summary>
        /// this method load contents such as models, fonts and sounds and conects them to this state parameters
        /// </summary>
        protected override void LoadContent()
        {
            // this playlist plays the "voice in the head" instructions for the player.
            string[] playList = { "Voice 005", "Voice 006" };
            //soundManager.StartPlayList(playList, 0);
            // soundManager.RepeatPlayList = false; // play the intructions only one time.

            spriteBatch = new SpriteBatch(GraphicsDevice);
            // font is load for printing on screen for player or for debug.
            announceFont = Game.Content.Load<SpriteFont>(@"Fonts\StartMenuFont");

            Hallway.model = Game.Content.Load<Model>(@"Models\state1Hallway\leather_tunnel2");
            Hallway.effect = Hallway.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            Hallway.Transform = Matrix.CreateTranslation(new Vector3(0, 0, 0)) *Matrix.CreateRotationY(1.5f)* world;

            camera.CreateLookAt(); // creates first look
            base.LoadContent();
        }

        /// <summary>
        /// update every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            

            //if finished the level successfully ->end level
            if (finishedState)
            {
                // moving the user to the Finish Stage Menu State
                StateManager.ChangeState(OurGame.FinishStageMenuState.Value);
            }

            // if player press p -> game paused.
            if (Input.KeyboardHandler.WasKeyPressed(Keys.P))
            {
                // Push the user to the distruption menu State
                StateManager.PushState(OurGame.DistruptionMenuState.Value);
            }
            // update camera
            camera.Update(gameTime);
            // creat bounding sphere to the updated camera position
            cameraBS = new BoundingSphere(camera.calculatedPosition, 0.6f);

            // if the player can walk camera position will get update
            camera.CameraPosition = camera.calculatedPosition; //can move
            camera.CreateLookAt();

            // player gets its updated position
            player1.Position = camera.CameraPosition;



            base.Update(gameTime);
        }

        /// <summary>
        /// draw every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {

            GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again

            GraphicsDevice.RasterizerState = solidRaster;
            BasicEffect effectToUpdate;
            
            effectToUpdate = Hallway.effect;

            effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
            effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
            effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
            effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
            effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
            effectToUpdate.DirectionalLight0.Enabled = true;
            effectToUpdate.TextureEnabled = true;
            effectToUpdate.LightingEnabled = true;

            Hallway.model.Draw(Hallway.Transform, camera.View, camera.Projection);



            // start spite batch to print on screen
            spriteBatch.Begin();




            // FOR TEST ONLY FOR DEBUG******************************************************************************
            string strPlayer = " playerModel x:" + camera.CameraPosition.X + "  y:" + camera.CameraPosition.Y + "  z:" + camera.CameraPosition.Z;
            string checking = "seconds:" + gameTime.ElapsedGameTime.TotalSeconds;
            string strTime = "total seconds : " + gameTime.TotalGameTime.TotalSeconds + " end time : " + endTime;
            // spriteBatch.DrawString(OurGame.announceFont, bandageString(), new Vector2(25, 25), Color.White);
            //spriteBatch.DrawString(OurGame.announceFont, camera.CameraToString(), new Vector2(25, 25), Color.White);
            //spriteBatch.DrawString(OurGame.announceFont, str, new Vector2(250, 250), Color.White);
            // spriteBatch.Draw(bandage, pos, new Rectangle(0, 0, bandage.Width, bandage.Height), Color.White, 0.0f, origin, scale, SpriteEffects.None, 1.0f);

            // close spritebatch draw
            spriteBatch.End();


            base.Draw(gameTime);
        }



        // checks states status
        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);

            // Change to visible if not at the top of the stack
            // This way, sub menus will appear on top of this menu
            if (StateManager.State != this.Value)
            {
                Visible = true;

            }


            // Sound Managing
            if (StateManager.State == this.Value && OurGame.EnableMusic)
                soundManager.ResumeSong();
            else
                soundManager.PauseSong();


            this.playSound = OurGame.EnableSoundFx;
        }

        // Build Bounding Box
        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }


    }
}
