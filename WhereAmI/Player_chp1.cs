using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    /// Player_chp1 is a drawable class that controls the "player" in the games first level.
    /// no player model due to first person view.
    /// the player object holds the camera position.
    /// this class determines metods and model that will be used in the rest of the game but not in the first level.

    public class Player_chp1 : Microsoft.Xna.Framework.DrawableGameComponent
    {

        public SpriteBatch spriteBatch;

        public Camera_2 Camera { get; set; }
        public Matrix World { get; set; }

        public Vector3 Position { get; set; }
        public Matrix Transform { get; set; } = Matrix.Identity;
        public Matrix Translation { get; set; } = Matrix.Identity;

        public Model player1model;
        public BasicEffect playerEffect;
        public RasterizerState wireframeRaster, solidRaster;

        public BoundingSphere bsFirstExit;
        float distanceToCamera = 5f, heightToCamera = -2.5f;

        BoundingFrustum bf;
        

        public Player_chp1(Game game)
            : base(game)
        {
      

            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;
            
        }

        protected override void LoadContent()
        {

            player1model = Game.Content.Load<Model>(@"Models\Earth");
            playerEffect = player1model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            Transform = Matrix.CreateScale(0.5f);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            bf = new BoundingFrustum(Camera.View);
            // player position is calculated by camera position.
            Position = Camera.CameraPosition + (new Vector3(Camera.cameraDirection.X * distanceToCamera, heightToCamera, Camera.cameraDirection.Z * distanceToCamera));
            Translation = Transform * Matrix.CreateTranslation(Position);


            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black); // black background

            GraphicsDevice.RasterizerState = solidRaster;

            if (player1model != null)
            {
                BasicEffect effectToUpdate;
                effectToUpdate = playerEffect;


                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;


                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                player1model.Draw(Translation, Camera.View, Camera.Projection);

            }

        }

        public void playerHoldMazrek(bool b)
        {
            // drawing mazrek next to camera
            if (b)
            {
                player1model = Game.Content.Load<Model>(@"Models\handMazrek");
                playerEffect = player1model.Meshes[0].MeshParts[0].Effect as BasicEffect;
                Transform = Matrix.CreateScale(2f);
            }
            //removing mazrek draw from camera, drawing mazrek in the vein
            else
            {
                //player1model = Game.Content.Load<Model>(@"Models\Earth");
                playerEffect = player1model.Meshes[0].MeshParts[0].Effect as BasicEffect;
                Transform = Matrix.CreateScale(0.5f);
            }
        }

        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }

    }
}
