using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using XELibrary;

namespace WhereAmI
{
    /// Cave is a drawable class that controls the cave area in the games first level.
    /// the class draws the cave model during the game.
    /// if the the player managed to stick the syringe in the vein (finish the state) 
    /// the cave class will draw its model with and without lightning for 20 seconds.

    public class Cave : Microsoft.Xna.Framework.DrawableGameComponent
    {

        public Camera_2 Camera { get; set; }
        public Matrix World { get; set; }
        private Matrix direction { get; set; }

        public Vector3 Position { get; set; } = Vector3.Zero;
        public Matrix Transform { get; set; } = Matrix.Identity;


        public RasterizerState wireframeRaster, solidRaster;

        public Game1.ModelClass cave = new Game1.ModelClass();  //Cave

        public List<Game1.ModelClass> modelsInCave = new List<Game1.ModelClass>();

        public bool endState = false;
        bool isLight = false;
        int framesCountAtEnd = 0;

        public Cave(Game game)
            : base(game)
        {
            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;


        }

        protected override void LoadContent()
        {
            cave.model = Game.Content.Load<Model>(@"Models\the_cave");
            cave.effect = cave.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            cave.position = new Vector3(0, -1.2f, 0);
            cave.Transform = Matrix.CreateTranslation(cave.position) * Matrix.CreateScale(new Vector3(4, 2, 4)) * World;

            modelsInCave.Add(cave);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again

            GraphicsDevice.RasterizerState = solidRaster;
            //GraphicsDevice.RasterizerState = wireframeRaster;
            BasicEffect effectToUpdate;
            if (endState)
            {
                
                framesCountAtEnd++;
                foreach (var model in modelsInCave)
                {
                    effectToUpdate = model.effect;


                    effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                    effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                    effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                    effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                    effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                    effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                    effectToUpdate.DirectionalLight0.Enabled = true;
                    effectToUpdate.TextureEnabled = true;
                    if (framesCountAtEnd%30==0)
                    {
                        isLight = !isLight;
                        effectToUpdate.LightingEnabled = isLight;
                    }

                    model.model.Draw(model.Transform, Camera.View, Camera.Projection);
                }
            }
            else
            {
                foreach (var model in modelsInCave)
                {
                    effectToUpdate = model.effect;


                    effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                    effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                    effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                    effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                    effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                    effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                    effectToUpdate.DirectionalLight0.Enabled = true;
                    effectToUpdate.TextureEnabled = true;
                    effectToUpdate.LightingEnabled = true;

                    model.model.Draw(model.Transform, Camera.View, Camera.Projection);
                }
            }
            
        }

        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }




    }
}
