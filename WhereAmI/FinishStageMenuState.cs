﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    public sealed class FinishStageMenuState : BaseGameState, IFinishStageMenuState
    {
        private Texture2D texture;
        ISoundManager soundManager;
        private SpriteFont font;
        private int selected;
        private string headLine = "You Finished Succesfuly!\nDo you have the \ncourage to continue?";
        private string[] entries = { "Continue The Story", "Exit" };

        public FinishStageMenuState(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IFinishStageMenuState), this);

            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
            soundManager.StopSong();
            selected = 0;
        }

        public override void Update(GameTime gameTime)
        {
            if (Input.KeyboardHandler.WasKeyPressed(Keys.Up))
                selected--;
            if (Input.KeyboardHandler.WasKeyPressed(Keys.Down))
                selected++;

            if (selected < 0)
                selected = entries.Length - 1;
            if (selected > entries.Length - 1)
                selected = 0;

            if (Input.KeyboardHandler.WasKeyPressed(Keys.Enter))
            {
                switch (selected)
                {
                    case 0:
                        // Go to next chapter
                        if (OurGame.currPlayStateNum == 2)
                        {
                            StateManager.ChangeState(OurGame.StartMenuState.Value); // move to start menu- no more stages.
                        }
                        else if (OurGame.currPlayStateNum == 1)
                        {
                            StateManager.ChangeState(OurGame.Chapter2State.Value); // move to chapter 2
                            OurGame.currPlayStateNum = 2;
                        }

                        break;
                    case 1:
                        Game.Exit();
                        break;
                }
            }

            base.Update(gameTime);
        }

        protected override void LoadContent()
        {
            texture = Content.Load<Texture2D>(@"Textures\Capture");
            font = Content.Load<SpriteFont>(@"Fonts\MenuFont");
        }

        public override void Draw(GameTime gameTime)
        {
            OurGame.spriteBatch.Begin();
            int view_width = Game.GraphicsDevice.Viewport.Width;
            int view_height = Game.GraphicsDevice.Viewport.Height;
            Vector2 pos = new Vector2(view_width / 2, view_height / 2);
            Vector2 origin = new Vector2(texture.Width / 2, texture.Height / 2);
            Vector2 scale1 = new Vector2((float)view_width / texture.Width, (float)view_height / texture.Height);


            OurGame.spriteBatch.Draw(texture, pos, new Rectangle(0, 0, texture.Width, texture.Height), Color.White, 0.0f, origin, scale1, SpriteEffects.None, 1.0f);
            Vector2 currPos = new Vector2(100, pos.Y / 2);
            // Draw Text
            OurGame.spriteBatch.DrawString(font, headLine, new Vector2(50, 50), Color.DarkSalmon);
            currPos.Y += font.LineSpacing * 3;
            for (int i = 0; i < entries.Length; i++)
            {
                Color color;
                float scale;

                if (i == selected)
                {
                    double time = gameTime.TotalGameTime.TotalSeconds;
                    float pulsate = (float)Math.Sin(time * 12) + 1;
                    color = Color.SaddleBrown;
                    scale = 1 + pulsate * 0.05f;
                }
                else
                {
                    color = Color.Black;
                    scale = 1;
                }

                Vector2 fontOrigin = new Vector2(0, font.LineSpacing / 2);
                Vector2 shadowPos = new Vector2(currPos.X - 2, currPos.Y - 2);

                // Draw Shadow
                OurGame.spriteBatch.DrawString(font, entries[i], shadowPos, Color.Silver, 0.0f, fontOrigin, scale, SpriteEffects.None, 0);

                // Draw Text
                OurGame.spriteBatch.DrawString(font, entries[i], currPos, color, 0.0f, fontOrigin, scale, SpriteEffects.None, 0);

                currPos.Y += font.LineSpacing;
            }
            OurGame.spriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);

            // Change to visible if not at the top of the stack
            // This way, sub menus will appear on top of this menu
            if (StateManager.State != this.Value)
                Visible = true;





        }

    }
}
