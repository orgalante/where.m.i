﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;

namespace WhereAmI
{
    /// Chapter1State is the state that controls every aspect of the first level in the game.
    /// it has a cave object as the level world, a monster object to hide from and a camera object to draw the level from first person view.
    /// the state controls actions between his objects, the level game play and moving to other states if has to or finished successfully.
    /// in order to pass this level the player has to find a syringe in the cave, then try to grab it, and then find the vein in the 
    /// cave and try to get to it and stick the syringe in it. if the player succeedes in doing that without getting caught by the monster
    /// he passes that level.

    public sealed class Chapter1State : BaseGameState, IChapter1State
    {
        // chapter1 parameters 3d
        public Camera_2 camera; //
        public SpriteBatch spriteBatch;
        private SpriteFont announceFont;
        Cave cave;
        Player_chp1 player1;
        Monster_chp1 monster1;

        Matrix world;

        //bandage parameters
        private Texture2D bandage;
        private Texture2D[] bandageBG = new Texture2D[20];
        int pressedInRow = 0;
        int movesForBandage = 0;
        string[] bandage_strs = new string[20];
        int bandageFrame = 0;


        private Texture2D mazrekPng;
        Boolean removeBandage = false;
        Boolean gotMazrek = false;
        Boolean usedMazrek = false;

        BoundingSphere cameraBS;

        //Mazrek 
        public Game1.ModelClass mazrek = new Game1.ModelClass();
        Model vrid;

        public BasicEffect vrideffect { get; private set; }
        public Matrix vridTransform { get; private set; }
        public bool iCan = true;

        BoundingBox vridBB;

        ISoundManager soundManager;

        bool canMove = false;
        private double endTime = 0;
        bool finishedState = false;
        private bool playMusic;
        private bool playSound;

        short maxDis = 20;
        short circle1Radius = 128;
        short circle2Radius = 278;
        short circle3Radius = 470;
        float curCalCircleRadius;
        float disFromCircle1Radius;
        float disFromCircle2Radius;
        float disFromCircle3Radius;
        BoundingSphere round1BS = new BoundingSphere(new Vector3(-15, 50, -121), 50);
        BoundingSphere round2BS = new BoundingSphere(new Vector3(210, 50, 238), 70);
        BoundingSphere MazrekBS = new BoundingSphere(new Vector3(-156, 50,181), 25);
        private bool stopChecking = false;


        BoundingSphere SyringeS1 = new BoundingSphere(new Vector3(-217, 50, 30), 40);
        BoundingSphere SyringeS2 = new BoundingSphere(new Vector3(50, 50, 211), 40);

        // CHAPTER1 STATE
        public Chapter1State(Game game)
            : base(game)
        {
            game.Services.AddService(typeof(IChapter1State), this); //add this state to game service.

            // Sound
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));

            // Camera
            Vector3 camPosition = new Vector3(0f, 50f, 0f); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-30, 50, -195); // position of target to look at in world
            camera = new Camera_2(game, camPosition, camLookTo); // camera first look initialize

            // World 
            world = Matrix.Identity; // Scale everything to a smaller size

            // Player1 build
            player1 = new Player_chp1(game);
            OurGame.Components.Add(player1);
            player1.Camera = camera;
            player1.World = world;

            // Monster1 build
            monster1 = new Monster_chp1(game);
            OurGame.Components.Add(monster1);
            monster1.Position = new Vector3(160, 0, 160);
            monster1.Camera = camera;
            monster1.World = world;
            monster1.playerPosition = player1.Position;

            // Cave build
            cave = new Cave(game);
            OurGame.Components.Add(cave);
            cave.Camera = camera;
            cave.World = world;

            // add Bandage
            Bandage bandage = new Bandage(game);


        }

        /// <summary>
        /// this method reset the state parameters to its initial values.
        /// </summary>
        public override void Initialize()
        {
            stopChecking = false;
            removeBandage = false;
            monster1.Initialize();
            canMove = false;
            endTime = 0;
            monster1.attacked = false;
            string[] playList = { "Survive", "Bandages", "Gore Ambience Sketch" };
            soundManager.StartPlayList(playList, 0);
            soundManager.RepeatPlayList = false;
            // Camera
            camera.Dispose();
            Vector3 camPosition = new Vector3(0f, 50f, 0f); // position of camera/player in world
            Vector3 camLookTo = new Vector3(-30, 50, -195); // position of target to look at in world
            camera = new Camera_2(OurGame, camPosition, camLookTo); // camera first initialize

            // Player1 build
            player1.Camera = camera;
            player1.Position = camera.CameraPosition;

            // Monster1 build
            monster1.Position = new Vector3(160, 0, 160);
            monster1.Camera = camera;
            monster1.playerPosition = player1.Position;

            // Cave build
            cave.Camera = camera;
            base.Initialize();
        }

        /// <summary>
        /// this method load contents such as models, fonts and sounds and conects them to this state parameters
        /// </summary>
        protected override void LoadContent()
        {

            for (int i = 1; i < 21; i++)
            {
                if (i < 10)
                    bandage_strs[i - 1] = @"Textures\(bandage)_000" + i;
                else
                    bandage_strs[i - 1] = @"Textures\(bandage)_00" + i;

                bandageBG[i - 1] = Content.Load<Texture2D>(bandage_strs[i - 1]);
            }
            // bandage texture
            bandage = Content.Load<Texture2D>(@"Textures\(bandage)_0001");


            // this playlist plays the "voice in the head" instructions for the player.
            string[] playList = { "Survive", "Bandages", "Gore Ambience Sketch" };
            //soundManager.StartPlayList(playList, 0);
            soundManager.RepeatPlayList = false; // play the intructions only one time.

            spriteBatch = new SpriteBatch(GraphicsDevice);
            // font is load for printing on screen for player or for debug.
            announceFont = Game.Content.Load<SpriteFont>(@"Fonts\StartMenuFont");

            // syringe texture - player grabs it
            mazrekPng = Game.Content.Load<Texture2D>(@"Textures\mazrekPng");
            // syringe model - in the cave
            mazrek.model = Game.Content.Load<Model>(@"Models\Mazrek"); //load the model
            mazrek.effect = mazrek.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
            mazrek.Transform = Matrix.CreateScale(2f) * Matrix.CreateTranslation(new Vector3(-50, 0, 120)) * world;
            // the bounding box of the syringe model - lets us detect collusion with player/camera
            mazrek.BB = BuildBoundingBox(mazrek.model, Matrix.CreateScale(4f) * world);

            // the vein model is used only to build a bounding box for it and detect a collusion
            vrid = Game.Content.Load<Model>(@"Models\vrid");
            vrideffect = vrid.Meshes[0].MeshParts[0].Effect as BasicEffect;
            vridTransform = Matrix.CreateScale(4.0f) * world;
            vridBB = BuildBoundingBox(vrid, vridTransform);

            camera.CreateLookAt(); // creates first look
            base.LoadContent();
        }

        /// <summary>
        /// update every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            ///this condition contorls that the player will hear voice 005 instructions without moving, 
            ///then he will hear voice 006 and could move but both sounds will be played only once.
            ///if both sounds finished playing then the state "song" is played reapitidly.
            if (!stopChecking)
            {
                // Sound Managing
                if (StateManager.State == this.Value && OurGame.EnableMusic)
                {
                    //player can walk only after first voice ended
                    if (soundManager.IsSongPlaying("Gore Ambience Sketch"))
                    {
                        soundManager.StopSong();
                        camera.canWalk = true;

                        //player can move now but will hear instruction only once
                        string[] playList = { "Gore Ambience Sketch" };
                        soundManager.StartPlayList(playList, 0);
                        soundManager.RepeatPlayList = true;
                        stopChecking = true;
                    }
                }
                canMove = true;
            }



            //if finished the level successfully ->end level
            if (finishedState)
            {
                monster1.paused = true; //so the monster will stop moving
                // moving the user to the Finish Stage Menu State
                soundManager.StopPlayList();
                StateManager.ChangeState(OurGame.FinishStageMenuState.Value);
            }
            // if monster model touches the player-> game ended
            if (monster1.attacked)
            {
                soundManager.StopPlayList();
                StateManager.PopState();
                StateManager.ChangeState(OurGame.StartMenuState.Value);
            }
            // if player press p -> game paused.
            if (Input.KeyboardHandler.WasKeyPressed(Keys.P))
            {
                // Push the user to the distruption menu State
                StateManager.PushState(OurGame.DistruptionMenuState.Value);
            }

            // update camera
            camera.Update(gameTime);
            // creat bounding sphere to the updated camera position
            cameraBS = new BoundingSphere(camera.calculatedPosition, 0.6f);



            if (Input.KeyboardHandler.WasKeyPressed(Keys.R) && bandageFrame < 19)
            {
                bandageFrame += 1;
                bandage = bandageBG[bandageFrame];

                // when pressed R the bandage texture is off, monster start to walk
                if (bandageFrame == 19)
                {
                    removeBandage = true;
                    monster1.paused = false;
                }
                else
                {
                    monster1.paused = true;
                }

            }



            //we want to check mazrekBB only if player never took it
            if (!gotMazrek)
            {
                if (cameraBS.Intersects(MazrekBS))
                {
                    gotMazrek = true;
                    //if player got the mazrek, draw it as if player is holding it
                    player1.playerHoldMazrek(true);
                    iCan = true;
                }



                if (cameraBS.Intersects(SyringeS1) && iCan)
                {
                    soundManager.Play("Syringe", 1f); // plays Syringe sound
                    iCan = false;
                }

                if (cameraBS.Intersects(SyringeS2) && !iCan)
                {
                    soundManager.Play("Syringe", 1f); // plays Syringe sound
                    iCan = true;
                }
            }

            //we want to check vridBB only if player got the mazrek
            else if (!usedMazrek && gotMazrek)
            {
                if (cameraBS.Intersects(vridBB))
                {
                    usedMazrek = true;
                    //if player got the mazrek, draw it as if player used it on the Vein
                    player1.playerHoldMazrek(false);
                    mazrek.model = Game.Content.Load<Model>(@"Models\vridMazrek");
                    mazrek.effect = mazrek.model.Meshes[0].MeshParts[0].Effect as BasicEffect;
                    mazrek.Transform = Matrix.CreateScale(4f) * world * Matrix.CreateTranslation(new Vector3(0, -10f, 0));
                    endTime = gameTime.TotalGameTime.TotalSeconds;
                }

                if (cameraBS.Intersects(round2BS) && iCan)
                {
                    soundManager.Play("MustDie", 1f); // plays Syringe sound
                    iCan = false; // in order to play it once
                }
            }
            //level finished successfully!
            else if (usedMazrek)
            {
                // player wait 20 seconds so the syringe can work on vein
                if (gameTime.TotalGameTime.TotalSeconds - endTime < 20)
                {
                    cave.endState = true;
                }
                // when the 20 seconds ends, update the parameters.
                else
                {
                    cave.endState = false;
                    finishedState = true;
                }
            }

            /// the circles are reference to the shape of the cave model rocks.
            /// circle1 is the most inner circle and so on.
            /// used x^2 + y^2 = r^2 function to determin the circle 
            curCalCircleRadius = (float)(Math.Sqrt((Math.Pow(camera.calculatedPosition.X, 2) + Math.Pow(camera.calculatedPosition.Z, 2))));
            disFromCircle1Radius = Math.Abs(curCalCircleRadius - circle1Radius);
            disFromCircle2Radius = Math.Abs(curCalCircleRadius - circle2Radius);
            disFromCircle3Radius = Math.Abs(curCalCircleRadius - circle3Radius);
            bool awayFromRocks = disFromCircle1Radius > maxDis && disFromCircle2Radius > maxDis && disFromCircle3Radius > maxDis;
            // roundBS is the exit position of that circle in the cave.
            // if player is on circle function but is on one of the roundBS, he can walk
            bool canPass = round2BS.Intersects(cameraBS) || round1BS.Intersects(cameraBS);
            bool cullusion = !awayFromRocks && !canPass;
            if (canMove)
            {
                // if there is a collusion , player can't move so camera position won't get update
                if (!cullusion)
                {
                    // if the player can walk camera position will get update
                    camera.CameraPosition = camera.calculatedPosition; //can move
                    camera.stepHeight += 0.05f;
                }
                // create the new look we the updated or old camera position
                camera.CreateLookAt();
            }

            // player gets its updated position
            player1.Position = camera.CameraPosition;
            // monster gets player position
            monster1.playerPosition = camera.CameraPosition;



            base.Update(gameTime);
        }

        /// <summary>
        /// draw every frame of this state
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {

            monster1.Draw(gameTime);
            cave.Draw(gameTime);

            // start spite batch to print on screen
            spriteBatch.Begin();

            // draw player holds mazrek
            if (gotMazrek && !usedMazrek)
                DrawMazrek();


            // FOR TEST ONLY FOR DEBUG******************************************************************************
            string strPlayer = " playerModel x:" + camera.CameraPosition.X + "  y:" + camera.CameraPosition.Y + "  z:" + camera.CameraPosition.Z;
            string strMonster = " Monster x:" + monster1.Position.X + "  y:" + monster1.Position.Y + "  z:" + monster1.Position.Z;
            string checking = "seconds:" + gameTime.ElapsedGameTime.TotalSeconds;
            string strAngle = "angle : " + monster1.angle;
            string strTime = "total seconds : " + gameTime.TotalGameTime.TotalSeconds + " end time : " + endTime;
            string disfrompl = "dis : " + monster1.disFromPlayer + " ply position mon: " + monster1.playerPosition;
            // spriteBatch.DrawString(OurGame.announceFont, bandageString(), new Vector2(25, 25), Color.White);
           // spriteBatch.DrawString(OurGame.announceFont, camera.CameraToString(), new Vector2(25, 25), Color.White);
            //spriteBatch.DrawString(OurGame.announceFont, str, new Vector2(250, 250), Color.White);
            // spriteBatch.Draw(bandage, pos, new Rectangle(0, 0, bandage.Width, bandage.Height), Color.White, 0.0f, origin, scale, SpriteEffects.None, 1.0f);



            //syringe draw standing in cave or in vein
            if (!gotMazrek || usedMazrek)
            {
                BasicEffect effectToUpdate;
                effectToUpdate = mazrek.effect;

                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;
                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                mazrek.model.Draw(mazrek.Transform, camera.View, camera.Projection);
            }

            // draw Bandage
            if (!removeBandage)
                DrawBandage();

            // close spritebatch draw
            spriteBatch.End();

            base.Draw(gameTime);
        }

        // draw bandage texture and text
        public void DrawBandage()
        {

            // Move forward and backward
            float y = (float)Math.Sin(20f * 50 * (Math.PI / 180)) * 0.05f;

            Vector2 pos = new Vector2(Game.GraphicsDevice.Viewport.Width / 2, (Game.GraphicsDevice.Viewport.Height / 2) + y);

            //Vector2 pos = new Vector2(Game.GraphicsDevice.Viewport.Width / 2,Game.GraphicsDevice.Viewport.Height / 2);
            Vector2 origin = new Vector2(bandage.Width / 2,
                                         (bandage.Height / 2) + y);
            Vector2 scale = new Vector2((float)Game.GraphicsDevice.Viewport.Width / bandage.Width,
                                        ((float)Game.GraphicsDevice.Viewport.Height / bandage.Height));
            // draw Bandage texture
            spriteBatch.Draw(bandage, pos, new Rectangle(0, 0, bandage.Width, bandage.Height + (int)y), Color.White, 0.0f, origin, scale, SpriteEffects.None, 1.0f);

            if (camera.canWalk)
                // draw bandage string
                spriteBatch.DrawString(announceFont, bandageString(), new Vector2(25, 25), Color.Red);
        }
        // draw Mazrek that is hold by player
        public void DrawMazrek()
        {
            int view_width = Game.GraphicsDevice.Viewport.Width;
            int view_height = Game.GraphicsDevice.Viewport.Height;
            Vector2 pos = new Vector2(view_width / 2, view_height / 2);
            Vector2 origin = new Vector2(mazrekPng.Width / 2, mazrekPng.Height / 2);
            Vector2 scale = new Vector2((float)view_width / mazrekPng.Width, (float)view_height / mazrekPng.Height);
            // draw Mazrek that is hold by player
            spriteBatch.Draw(mazrekPng, pos, new Rectangle(0, 0, mazrekPng.Width, mazrekPng.Height), Color.White, 0.0f, origin, scale, SpriteEffects.None, 1.0f);
        }
        // for debug 
        public string positionString()
        {
            string str = " calculated x " + (int)camera.calculatedPosition.X + " z " + (int)camera.calculatedPosition.Z + "\n";
            str += "calc radius " + curCalCircleRadius + "\n";
            str += "from 1 :" + disFromCircle1Radius + "\n";
            str += "from 2 :" + disFromCircle2Radius + "\n";
            str += "from 3 :" + disFromCircle3Radius + "\n";
            return str;
        }
        // the bandage string for the player
        public string bandageString()
        {
            return "Press R to Remove the Bandage \nKeep in mind it releases her...";
        }

        // checks states status
        protected override void StateChanged(object sender, EventArgs e)
        {
            base.StateChanged(sender, e);

            // Change to visible if not at the top of the stack
            // This way, sub menus will appear on top of this menu
            if (StateManager.State != this.Value)
            {
                Visible = true;
                monster1.paused = true;
            }
            else
                monster1.paused = false;

            // Sound Managing
            if (StateManager.State == this.Value && OurGame.EnableMusic)
                soundManager.ResumeSong();
            else
                soundManager.PauseSong();

            monster1.playSound = OurGame.EnableSoundFx;
            this.playSound = OurGame.EnableSoundFx;
        }

        // Build Bounding Box
        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }


    }
}
