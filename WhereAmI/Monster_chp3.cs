using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using XELibrary;

namespace WhereAmI
{
    /// Monster_chp1 is a drawable class that controls every aspect of the "monster" in the games first level.
    /// it has its informative parameters such as his location, the player location and so on...
    /// the class determines the monster model patrol path in cave and its behavior when reaching too close to the player.
    /// the class determines sounds such as attack or breath sound.
    /// the monster can "attack" the player by coming to it and when touching it the game is over.

    public class Monster_chp3 : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public double lastBreathSound = 0;
        public double lastAttackSound = 0;
        public double lastScreamSound = 0;
        public Camera_2 Camera { get; set; }
        public Matrix World { get; set; }

        public Vector3 Position { get; set; }
        public Matrix Transform { get; set; } = Matrix.Identity;

        private BoundingFrustum playerBF;

        public Matrix Translation { get; set; } = Matrix.Identity;

        public Vector3 destination { get; set; }
        bool moving = false;
        private bool chasingP;


        public bool playSound = false;

        public Vector3 playerPosition = new Vector3();
        public float disFromPlayer;

        public Model monsterModel;
        public BasicEffect monsterEffect;
        public BoundingBox monsterBB;
        public RasterizerState wireframeRaster, solidRaster;
        ISoundManager soundManager;
        public bool attacked = false;

        public double angle = 60;
        private double secFromStart = 0;

        public bool paused = false;

        public Monster_chp3(Game game)
            : base(game)
        {
            soundManager = (ISoundManager)game.Services.GetService(typeof(ISoundManager));
            solidRaster = new RasterizerState();
            solidRaster.FillMode = FillMode.Solid;
            wireframeRaster = new RasterizerState();
            wireframeRaster.FillMode = FillMode.WireFrame;
        }

        public override void Initialize()
        {
            angle = 60;
            secFromStart = 0;
            attacked = false;
            moving = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            monsterModel = Game.Content.Load<Model>(@"Models\Earth");
            monsterEffect = monsterModel.Meshes[0].MeshParts[0].Effect as BasicEffect;
            Transform = Matrix.CreateScale(1, 1, 1) * Matrix.CreateRotationY(4.3f);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            
            /// need to check the player position/camera position !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            disFromPlayer = (float)Vector3.Distance(playerPosition, this.Position);
            //playerBF = new BoundingFrustum(Matrix.CreateTranslation(Camera.CameraPosition * Camera.cameraDirection));
            Translation = Matrix.CreateRotationY(0.15f)* Matrix.CreateTranslation(Position) * World;
            monsterBB = BuildBoundingBox(monsterModel, Translation);


            if (!paused && !attacked)
            {
                secFromStart = gameTime.TotalGameTime.TotalSeconds;
                if (disFromPlayer < 150)
                {
                    chasingP = true;
                    if (secFromStart - lastAttackSound > 2 && playSound)
                    {
                        soundManager.Play("Attack-06", 1f); // plays monster attack sound
                        lastAttackSound = secFromStart;
                    }
                    moveTo(playerPosition);
                }

                setNextStep();
                float volume = 1 - ((disFromPlayer - 150) / 200);
                //means about every 9 seconds play a breath sound
                if (secFromStart - lastBreathSound > 9 && playSound)
                {
                    if (disFromPlayer < 350 && disFromPlayer > 150)
                    {
                        //makes the distance from player a factor of monster sound volume.

                        lastBreathSound = secFromStart;
                        soundManager.Play("Breath-03", volume); // plays monster breath sound
                    }
                }

                if (secFromStart - lastScreamSound > 15 && playSound && Camera.canWalk)
                {
                    if (disFromPlayer < 350 && disFromPlayer > 150)
                    {
                        lastScreamSound = secFromStart;
                        soundManager.Play("Attack-06", volume); // plays monster scream sound
                    }
                }
            }



            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black); // black background

            GraphicsDevice.RasterizerState = solidRaster;

            if (monsterModel != null)
            {
                BasicEffect effectToUpdate;
                effectToUpdate = monsterEffect;

                effectToUpdate.AmbientLightColor = new Vector3(0.30f, 0.30f, 0.30f);
                effectToUpdate.DiffuseColor = new Vector3(0.65f, 0.65f, 0.65f);
                effectToUpdate.SpecularColor = new Vector3(0.50f, 0.50f, 0.50f);
                effectToUpdate.DirectionalLight0.DiffuseColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.SpecularColor = new Vector3(1.00f, 1.00f, 1.00f);
                effectToUpdate.DirectionalLight0.Direction = new Vector3(-1.0f, -0.2f, -1.0f);
                effectToUpdate.DirectionalLight0.Enabled = true;
                effectToUpdate.TextureEnabled = true;
                effectToUpdate.LightingEnabled = true;

                GraphicsDevice.DepthStencilState = DepthStencilState.Default; //make drawing 3d avaible again
                monsterModel.Draw(Translation, Camera.View, Camera.Projection);

            }

        }

        public override string ToString()
        {
            string str;
            str = "Monster:\nPosition: " + (int)Position.X + "," + (int)Position.Y + "," + (int)Position.Z + "\n";
            str += "P_position: " + (int)playerPosition.X + "," + (int)playerPosition.Y + "," + (int)playerPosition.Z + "\n";
            str += "distance_P: " + (int)disFromPlayer + "\n";
            str += "Angle: " + (int)angle + "\n";
            str += "sec: " + secFromStart + "\n";
            str += "lastBreath: " + lastBreathSound + "\n";
            str += "lastAttack: " + lastAttackSound;

            return str;
        }

        public void attack()
        {
            attacked = true;
        }

        public void setNextStep()
        {
            float maxDistance = 20f;
            angle += 0.05;
            float speed = 2;
            float heightLimit = 5;
            double calc = angle * (Math.PI / 180);
            //float y = (float)Math.Sin(speed * calc) * heightLimit;
            float y = 0;

            if (moving)
            {
                float elapsed = 0.1f;
                Vector3 direction = Vector3.Normalize(destination - Position);
                Position += direction * elapsed * speed;
                //Position += new Vector3(0, y, 0);
                if (Vector3.Distance(Position, destination) <= maxDistance)
                {
                    if (destination == playerPosition)
                        attack();
                    else
                    {
                        //Position = destination;
                        Position = destination + new Vector3(150, 0, 150);
                        moving = false;
                        angle = 60;// round entry/exit
                    }
                }
                else if (disFromPlayer > 150 && chasingP)
                {
                    moving = false;
                    chasingP = false;
                }
            }

        }

        public void moveTo(Vector3 to)
        {
            destination = to;
            moving = true;
        }

        public BoundingBox BuildBoundingBox(Model model, Matrix meshTransform)
        {
            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    // The stride is how big, in bytes, one vertex is in the vertex buffer
                    // We have to use this as we do not know the make up of the vertex
                    int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                    VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                    part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                    // Find minimum and maximum xyz values for this mesh part
                    Vector3 vertPosition = new Vector3();

                    for (int i = 0; i < vertexData.Length; i++)
                    {
                        vertPosition = vertexData[i].Position;

                        // update our values from this vertex
                        meshMin = Vector3.Min(meshMin, vertPosition);
                        meshMax = Vector3.Max(meshMax, vertPosition);
                    }
                }

                // transform by mesh bone matrix
                meshMin = Vector3.Transform(meshMin, meshTransform);
                meshMax = Vector3.Transform(meshMax, meshTransform);
            }
            // Create the bounding box
            BoundingBox box1 = new BoundingBox(meshMin, meshMax);
            return box1;

        }

    }
}
